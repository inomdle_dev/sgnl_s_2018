#ifndef _BHI160_H_
#define _BHI160_H_

void i2c_pre_initialize(void);
extern int bhi160_init(void);

//void step_counter_read( uint32_t *walk, uint32_t  *run, uint16_t *walk_time, uint16_t *run_time );
void step_counter_read( uint32_t *step_count, uint16_t *walk_time, uint16_t *run_time );
void bhi160_read_data( void );

#endif //_BHI160_H_