/*!
  * Copyright (C) Robert Bosch. All Rights Reserved.
  *
  *
  * <Disclaimer>
  *
  * Common: Bosch Sensortec products are developed for the consumer goods
  * industry. They may only be used within the parameters of the respective valid
  * product data sheet.  Bosch Sensortec products are provided with the express
  * understanding that there is no warranty of fitness for a particular purpose.
  * They are not fit for use in life-sustaining, safety or security sensitive
  * systems or any system or device that may lead to bodily harm or property
  * damage if the system or device malfunctions. In addition, Bosch Sensortec
  * products are not fit for use in products which interact with motor vehicle
  * systems.  The resale and/or use of products are at the purchaser's own risk
  * and his own responsibility. The examination of fitness for the intended use
  * is the sole responsibility of the Purchaser.
  *
  * The purchaser shall indemnify Bosch Sensortec from all third party claims,
  * including any claims for incidental, or consequential damages, arising from
  * any product use not covered by the parameters of the respective valid product
  * data sheet or not approved by Bosch Sensortec and reimburse Bosch Sensortec
  * for all costs in connection with such claims.
  *
  * The purchaser must monitor the market for the purchased products,
  * particularly with regard to product safety and inform Bosch Sensortec without
  * delay of all security relevant incidents.
  *
  * Engineering Samples are marked with an asterisk (*) or (e). Samples may vary
  * from the valid technical specifications of the product series. They are
  * therefore not intended or fit for resale to third parties or for use in end
  * products. Their sole purpose is internal client testing. The testing of an
  * engineering sample may in no way replace the testing of a product series.
  * Bosch Sensortec assumes no liability for the use of engineering samples. By
  * accepting the engineering samples, the Purchaser agrees to indemnify Bosch
  * Sensortec from all claims arising from the use of engineering samples.
  *
  * Special: This software module (hereinafter called "Software") and any
  * information on application-sheets (hereinafter called "Information") is
  * provided free of charge for the sole purpose to support your application
  * work. The Software and Information is subject to the following terms and
  * conditions:
  *
  * The Software is specifically designed for the exclusive use for Bosch
  * Sensortec products by personnel who have special experience and training. Do
  * not use this Software if you do not have the proper experience or training.
  *
  * This Software package is provided `` as is `` and without any expressed or
  * implied warranties, including without limitation, the implied warranties of
  * merchantability and fitness for a particular purpose.
  *
  * Bosch Sensortec and their representatives and agents deny any liability for
  * the functional impairment of this Software in terms of fitness, performance
  * and safety. Bosch Sensortec and their representatives and agents shall not be
  * liable for any direct or indirect damages or injury, except as otherwise
  * stipulated in mandatory applicable law.
  *
  * The Information provided is believed to be accurate and reliable. Bosch
  * Sensortec assumes no responsibility for the consequences of use of such
  * Information nor for any infringement of patents or other rights of third
  * parties which may result from its use. No license is granted by implication
  * or otherwise under any patent or patent rights of Bosch. Specifications
  * mentioned in the Information are subject to change without notice.
  *
  *
  * @file          activity_recognition_example.c
  *
  * @date          12/15/2016
  *
  * @brief         example to showcases the BHI160 activity recognition.
  *                 It records all of the user's activity into the non-wakeup FIFO and outputs it to the terminal window
  *                 Configuration 115200, 8N
  */

/********************************************************************************/
/*                                  HEADER FILES                                */
/********************************************************************************/

#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>

#include "nordic_common.h"
#include "nrf_delay.h"
#include "bhy_uc_driver.h"
/* for customer , to put the firmware array */
#include "BHIfw.h"

#include "user_struct.h"
#include "pattern_control.h"
	  
/********************************************************************************/
/*                                       MACROS                                 */
/********************************************************************************/

/* should be greater or equal to 69 bytes, page size (50) + maximum packet size(18) + 1 */
#define ARRAYSIZE            69
#define OUT_BUFFER_SIZE      100
#define MAX_PACKET_LENGTH    18


/*!
 * @brief This structure holds all setting for the console
 */
/********************************************************************************/
/*                                GLOBAL VARIABLES                              */
/********************************************************************************/
/* system timestamp */
static uint32_t bhy_system_timestamp = 0;

pedo_struct_t g_str_pedo;

/********************************************************************************/
/*                           STATIC FUNCTION DECLARATIONS                       */
/********************************************************************************/
static void sensors_callback(bhy_data_generic_t * sensor_data, bhy_virtual_sensor_t sensor_id);
static void timestamp_callback(bhy_data_scalar_u16_t *new_timestamp);


/********************************************************************************/
/*                                    FUNCTIONS                                 */
/********************************************************************************/

/*!
 * @brief     This function  issues 9 clock cycles on the SCL line
 *             so that all devices release the SDA line if they are holding it
 */
void i2c_pre_initialize(void)
{
    //ioport_set_pin_dir(EXT1_PIN_I2C_SCL, IOPORT_DIR_OUTPUT);

    //for (int8_t i = 0; i < 9; ++i)
    {
        //ioport_set_pin_level(EXT1_PIN_I2C_SCL, IOPORT_PIN_LEVEL_LOW);
        //nrf_delay_us(2);

        //ioport_set_pin_level(EXT1_PIN_I2C_SCL, IOPORT_PIN_LEVEL_HIGH);
        //nrf_delay_us(1);
    }
}


/*!
 * @brief This function is  callback function for acquring sensor datas
 *
 * @param[in]   sensor_data
 * @param[in]   sensor_id
 */
static void sensors_callback(bhy_data_generic_t * sensor_data, bhy_virtual_sensor_t sensor_id)
{
	uint16_t timestamp;
	uint32_t pedo_time;
    /* Since a timestamp is always sent before every new data, and that the callbacks   */
    /* are called while the parsing is done, then the system timestamp is always equal  */
    /* to the sample timestamp. (in callback mode only)   */
    timestamp = bhy_system_timestamp / 32000;

	if( sensor_id == VS_ID_STEP_COUNTER )
	{
		g_str_pedo.ui32_cur_step = sensor_data->data_scalar_u16.data;
		
		g_str_pedo.ui32_pedo_step = g_str_pedo.ui32_last_step + g_str_pedo.ui32_cur_step;
	}
	if( sensor_id == VS_ID_ACTIVITY )
	{
		if( g_str_app.flag.b1_longsit_status )
		{
			g_str_app.health.ui8_longsit_count = 0;
		}

		if (sensor_data->data_scalar_u16.data & 0x0002)//walking end
		{                                              
			g_str_pedo.ui32_walk_step += g_str_pedo.ui32_pedo_step - g_str_pedo.ui32_last_end_step;
			if (timestamp > g_str_pedo.ui16_start_walk_time)
			{
				g_str_pedo.ui16_walk_time += ( timestamp - g_str_pedo.ui16_start_walk_time );
			}
		}

		if (sensor_data->data_scalar_u16.data & 0x0004)//run end
		{                                              
			g_str_pedo.ui32_run_step += g_str_pedo.ui32_pedo_step - g_str_pedo.ui32_last_end_step;
			if (timestamp > g_str_pedo.ui16_start_run_time)
			{
				g_str_pedo.ui16_run_time += ( timestamp - g_str_pedo.ui16_start_run_time );
			}
		}                                              

		if (sensor_data->data_scalar_u16.data & 0x0008)//bike end
		{
			g_str_pedo.ui32_bike_step += g_str_pedo.ui32_pedo_step - g_str_pedo.ui32_last_end_step;
			if (timestamp > g_str_pedo.ui16_start_bike_time)
			{
				g_str_pedo.ui16_bike_time += ( timestamp - g_str_pedo.ui16_start_bike_time );
			}
		}                                              

		if (sensor_data->data_scalar_u16.data & 0x0010)//vehicle end
		{
			g_str_pedo.ui32_vehicle_step += g_str_pedo.ui32_pedo_step - g_str_pedo.ui32_last_end_step;
			if (timestamp > g_str_pedo.ui16_start_vehicle_time)
			{
				g_str_pedo.ui16_vehicle_time += ( timestamp - g_str_pedo.ui16_start_vehicle_time );
			}
		}                                              

		if( sensor_data->data_scalar_u16.data & 0x001E)//move_end
		{
			g_str_pedo.ui32_last_end_step = g_str_pedo.ui32_pedo_step;
			
			g_str_pedo.pedo_mode = PEDO_MOVE_STOP;
		}

				
		if (sensor_data->data_scalar_u16.data & 0x0001)//still end - move start
		{                                              
			g_str_pedo.ui16_start_move_time = timestamp;
			g_str_pedo.pedo_mode = PEDO_MOVE_START;
		}
		
		if( sensor_data->data_scalar_u16.data & 0x1E00)//activity_start
		{
			if( g_str_pedo.ui16_start_move_time )
			{
				if (timestamp > g_str_pedo.ui16_start_move_time)
				{
					g_str_pedo.ui16_pre_move_time += ( timestamp - g_str_pedo.ui16_start_move_time )>>1;
					g_str_pedo.ui16_start_move_time = timestamp;
				}
			}
			else
			{
				g_str_pedo.ui16_pre_move_time = 0;
			}
		}

		if (sensor_data->data_scalar_u16.data & 0x0100)//still start
		{
			if( g_str_pedo.ui16_start_move_time )
			{
				if (timestamp > g_str_pedo.ui16_start_move_time)
				{
					g_str_pedo.ui16_pre_move_time += ( timestamp - g_str_pedo.ui16_start_move_time )>>1;
					g_str_pedo.ui16_start_move_time = timestamp;
				}
			}
			g_str_pedo.pedo_mode = PEDO_MOVE_STOP;
		}

		if (sensor_data->data_scalar_u16.data & 0x0200)//walking start
		{
			g_str_pedo.ui16_start_walk_time = timestamp;
			
			if( g_str_pedo.ui16_pre_move_time > 0 )
			{
				g_str_pedo.ui16_walk_time += g_str_pedo.ui16_pre_move_time;
				g_str_pedo.ui16_pre_move_time = 0;
			}
			g_str_pedo.pedo_mode = PEDO_MOVE_WALK;
		}
		
		if (sensor_data->data_scalar_u16.data & 0x0400)//run start
		{
			g_str_pedo.ui16_start_run_time = timestamp;
			
			if( g_str_pedo.ui16_pre_move_time > 0 )
			{
				g_str_pedo.ui16_run_time += g_str_pedo.ui16_pre_move_time;
				g_str_pedo.ui16_pre_move_time = 0;
			}
			g_str_pedo.pedo_mode = PEDO_MOVE_RUN;
		}

		if (sensor_data->data_scalar_u16.data & 0x0800)//bike start
		{
			g_str_pedo.ui16_start_bike_time = timestamp;
			
			if( g_str_pedo.ui16_pre_move_time > 0 )
			{
				g_str_pedo.ui16_bike_time += g_str_pedo.ui16_pre_move_time;
				g_str_pedo.ui16_pre_move_time = 0;
			}
			g_str_pedo.pedo_mode = PEDO_MOVE_BIKE;
		}
		
		if (sensor_data->data_scalar_u16.data & 0x1000)//vehicle start
		{
			g_str_pedo.ui16_start_vehicle_time = timestamp;
			
			if( g_str_pedo.ui16_pre_move_time > 0 )
			{
				g_str_pedo.ui16_vehicle_time += g_str_pedo.ui16_pre_move_time;
				g_str_pedo.ui16_pre_move_time = 0;
			}
			g_str_pedo.pedo_mode = PEDO_MOVE_VEHICLE;
		}

		/* activity recognition is not time critical, so let's wait a little bit */
    	nrf_delay_ms(50);
	}

	if( g_str_app.flag.b1_act_time_status )
	{
		pedo_time = g_str_pedo.ui16_walk_time + g_str_pedo.ui16_vehicle_time
				  + g_str_pedo.ui16_run_time + g_str_pedo.ui16_bike_time;
		
		if( g_str_app.flag.b1_act_time_noti )
		{
			if( pedo_time > ( g_str_app.health.ui16_act_time * 60 ) )
			{
				//act_time noti
				g_str_app.flag.b1_act_time_noti = 0;
				pattern_start(PATTERN_HEALTH_NOTI);
			}
		}
	}
	
	if( g_str_app.flag.b1_act_step_status )
	{
		if( g_str_app.flag.b1_act_step_noti ) 
		{
			if( g_str_pedo.ui32_pedo_step > ( g_str_app.health.ui16_act_step * 1000 ) )
			{
				//act_step noti
				g_str_app.flag.b1_act_step_noti = 0;
				pattern_start(PATTERN_HEALTH_NOTI);
			}
		}
	}
	//UNUSED_VARIABLE(timestamp);
	//UNUSED_VARIABLE(out_buffer);
}

/*!
 * @brief This function is time stamp callback function that process data in fifo.
 *
 * @param[in]   new_timestamp
 */
static void timestamp_callback(bhy_data_scalar_u16_t *new_timestamp)
{
    /* updates the system timestamp */
    bhy_update_system_timestamp(new_timestamp, &bhy_system_timestamp);
}

/*!
 * @brief     main body function
 *
 * @retval   result for execution
 */
int bhi160_init(void)
{
    /* initializes the BHI160 and loads the RAM patch */
    if( bhy_driver_init(bhy_firmware_image/*, _bhi_fw_len*/) )
	{
		return 1;
	}

	//ready to interrupt
    /* wait for the interrupt pin to go down then up again */
    while (g_str_bit.b1_pedo_int)
    {
		__WFE();
		g_str_bit.b1_pedo_int = 0;
    }

    /* enables the activity recognition and assigns the callback */
	nrf_delay_ms(50);
 	bhy_enable_virtual_sensor(VS_TYPE_ACTIVITY_RECOGNITION, VS_NON_WAKEUP, 5, 0, VS_FLUSH_NONE, 0, 0);
	nrf_delay_ms(50);
    bhy_enable_virtual_sensor(VS_TYPE_STEP_COUNTER, VS_NON_WAKEUP, 1, 0, VS_FLUSH_NONE, 0, 0);
	nrf_delay_ms(50);
	bhy_install_sensor_callback(VS_TYPE_ACTIVITY_RECOGNITION, VS_NON_WAKEUP, sensors_callback);
	bhy_install_sensor_callback(VS_TYPE_STEP_COUNTER, VS_NON_WAKEUP, sensors_callback);
    bhy_install_timestamp_callback(VS_NON_WAKEUP, timestamp_callback);

	return 0;
}

//void step_counter_read( uint32_t *walk, uint32_t  *run, uint16_t *walk_time, uint16_t *run_time )
void step_counter_read( uint32_t *step_count, uint16_t *walk_time, uint16_t *run_time )
{
	uint16_t timestamp;
    /* Since a timestamp is always sent before every new data, and that the callbacks   */
    /* are called while the parsing is done, then the system timestamp is always equal  */
    /* to the sample timestamp. (in callback mode only)   */
    timestamp = bhy_system_timestamp / 32000;

	//*walk = g_str_pedo.ui32_walk_step + g_str_pedo.ui32_vehicle_step;
	//*run = g_str_pedo.ui32_run_step + g_str_pedo.ui32_bike_step;
	*step_count = g_str_pedo.ui32_pedo_step;
	
	if( g_str_pedo.pedo_mode == PEDO_MOVE_WALK )
	{
		if ( timestamp > g_str_pedo.ui16_start_walk_time )
		{
			g_str_pedo.ui16_walk_time += ( timestamp - g_str_pedo.ui16_start_walk_time );
		}
		g_str_pedo.ui16_start_walk_time = timestamp ;
	}
	
	if( g_str_pedo.pedo_mode == PEDO_MOVE_RUN )
	{
		if ( timestamp > g_str_pedo.ui16_start_run_time )
		{
			g_str_pedo.ui16_run_time += ( timestamp - g_str_pedo.ui16_start_run_time );
		}
		g_str_pedo.ui16_start_run_time = timestamp ;
	}
	
	if( g_str_pedo.pedo_mode == PEDO_MOVE_BIKE )
	{
		if ( timestamp > g_str_pedo.ui16_start_bike_time )
		{
			g_str_pedo.ui16_bike_time += ( timestamp - g_str_pedo.ui16_start_bike_time );
		}
		g_str_pedo.ui16_start_bike_time = timestamp ;
	}
	
	if( g_str_pedo.pedo_mode == PEDO_MOVE_VEHICLE )
	{
		if ( timestamp > g_str_pedo.ui16_start_vehicle_time )
		{
			g_str_pedo.ui16_vehicle_time += ( timestamp - g_str_pedo.ui16_start_vehicle_time );
		}
		g_str_pedo.ui16_start_vehicle_time = timestamp ;
	}

	*walk_time = g_str_pedo.ui16_walk_time + g_str_pedo.ui16_vehicle_time;
	*run_time = g_str_pedo.ui16_run_time + g_str_pedo.ui16_bike_time;
}

//read motion sensor data when motion interrupt
void bhi160_read_data( void )
{
    uint8_t                    array[ARRAYSIZE];
    uint8_t                    *fifoptr           = NULL;
    uint8_t                    bytes_left_in_fifo = 0;
    uint16_t                   bytes_remaining    = 0;
    uint16_t                   bytes_read         = 0;
    bhy_data_generic_t         fifo_packet;
    bhy_data_type_t            packet_type;
    BHY_RETURN_FUNCTION_TYPE   result;

	
	bhy_read_fifo(array+bytes_left_in_fifo, ARRAYSIZE - bytes_left_in_fifo, &bytes_read, &bytes_remaining);
	bytes_read         += bytes_left_in_fifo;
	fifoptr             = array;
	packet_type        = BHY_DATA_TYPE_PADDING;

	if (bytes_read)
	{
		do
		{
			/* this function will call callbacks that are registered */
			result = bhy_parse_next_fifo_packet(&fifoptr, &bytes_read, &fifo_packet, &packet_type);
			/* the logic here is that if doing a partial parsing of the fifo, then we should not parse        */
			/* the last 18 bytes (max length of a packet) so that we don't try to parse an incomplete   */
			/* packet                                                                                                            */
		} while ((result == BHY_SUCCESS) && (bytes_read > (bytes_remaining ? MAX_PACKET_LENGTH : 0)));

		bytes_left_in_fifo = 0;

		if (bytes_remaining)
		{
			/* shifts the remaining bytes to the beginning of the buffer */
			while (bytes_left_in_fifo < bytes_read)
			{
				array[bytes_left_in_fifo++] = *(fifoptr++);
			}
		}
	}
	else
	{
		/* activity recognition is not time critical, so let's wait a little bit */
    	//nrf_delay_ms(100);
	}
}

/** @}*/
