#ifndef _ANCS_FLASH_MANAGER_H_
#define _ANCS_FLASH_MANAGER_H_

/*!
 * @brief        This function do write flash for ancs package name
 *
 * @param[in]    package_name		address of app package name string
 * @param[in]    package_length		length of app package name
 * @param[in]    write_cnt			data index to be written
 *
 * @retval       N/A
 */
void ancs_flash_write( char * package_name, uint8_t u8_data_len, uint8_t write_cnt );

/*!
 * @brief        This function do erase flash data of blocked app
 *
 * @param[in]    package_name		address of app package name string
 * @param[in]    package_length		length of app package name
 *
 * @retval       N/A
 */
void ancs_flash_erase( char * package_name, uint8_t u8_data_len );

/*!
 * @brief        This function do find flash data of blocked app list
 *
 * @param[in]    package_name		address of app package name string
 * @param[in]    package_length		length of app package name
 *
 * @retval       result of excution - 0 : find, N : couldn't find, 0xEE : error
 */
uint8_t ancs_flash_find( char * p_package_name, uint8_t pakage_length );

/*!
 * @brief        This function do parsing data of blocked app name
 *
 * @param[in]    p_data				target data of parsing
 * @param[in]    ui8_data_len		length of target data
 *
 * @retval       N/A
 */
void ancs_data_parser( uint8_t * p_data, uint8_t ui8_data_len );

#endif //_ANCS_FLASH_MANAGER_H_