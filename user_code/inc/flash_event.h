#ifndef _FLASH_EVENT_H_
#define _FLASH_EVENT_H_

typedef enum user_flash_list_{
	USER_FLASH_APP_CONFIG = 0,
	USER_FLASH_PEDOMETER_HOUR = 1,
	USER_FLASH_PEDOMETER_DAILY = 2,
	USER_FLASH_ANCS_BLOCK = 3,
	PEER_MANAGER_FLASH_1 = 4,
	PEER_MANAGER_FLASH_2 = 5,
	PEER_MANAGER_FLASH_3 = 6,
	
	USER_FLASH_LIST = 7,
}user_flash_list_t;

extern void flash_init(void);

extern void user_flash_erase(user_flash_list_t list);
extern void user_flash_write( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );
extern void user_flash_read( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );

#endif //_FLASH_EVENT_H_