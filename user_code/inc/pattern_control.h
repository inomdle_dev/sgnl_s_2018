#ifndef _PATTERN_CONTROL_H_
#define _PATTERN_CONTROL_H_

#define PATTERN_INDEX	16

#define PATTERN_POWER_ON_CNT			205
#define PATTERN_POWER_OFF_CNT			165

#define PATTERN_BATT_GAUGE_CNT			225
#define PATTERN_BATT_LOW_CNT			185

#define PATTERN_BATT_CHARGING_CNT		105
#define PATTERN_BATT_FULL_CNT			150

#define PATTERN_WAIT_PAIRING_CNT		340
#define PATTERN_PAIRING_COMPLETE_CNT	175
#define PATTERN_DISCONN_TIMEOUT_CNT		200

#define PATTERN_DND_ON_CNT				125
#define PATTERN_DND_OFF_CNT				125

#define PATTERN_INCOMING_CALL_CNT		105

#define PATTERN_APP_NOTI_CNT			130

#define PATTERN_DEVICE_RESET_CNT		155

#define PATTERN_UNPAIRED_CNT		300
#define PATTERN_UNPAIR_CNT			165

#define PATTERN_BLE_CONN_CHK_CNT	200
#define PATTERN_DISCONN_TIMEOUT_CNT 200

#define PATTERN_FW_UPGRADE_CNT		150

typedef void(*p_pattern_arr)(void);

typedef void(*p_led_arr)(uint32_t);

extern p_pattern_arr p_pattern_func_arr[PATTERN_INDEX];

extern void pattern_timer_init(void);
extern void pattern_start(uint8_t u8_pattern_index);

extern void pattern_all_off( void );

#endif //_PATTERN_CONTROL_H_