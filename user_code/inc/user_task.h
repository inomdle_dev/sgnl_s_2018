#ifndef _USER_TASK_H_
#define _USER_TASK_H_

extern void user_init( void );
extern void user_task( void );
extern void func_btn_click( void );
extern void factory_reset( void );
#endif //_USER_TASK_H_