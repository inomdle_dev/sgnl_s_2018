/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#ifndef PIN_SGNL_H
#define PIN_SGNL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"
	
#if defined(BOARD_SGNL_S)
	
#if 0 //before
	#define SCL_PIN			0//ok
	#define PEDO_INT		1
	#define SDA_PIN			5//ok
	
	#define HAPTIC_OUT		6
	#define PMIC_INT		8

	#define LED_1          12//ok
	#define LED_2          14//ok
	#define LED_3          15//ok
	#define LED_4          16//ok
	#define LED_5          18//ok
	
	#define BUTTON_1		29
	#define BUTTON_2		25
	
	#define CHK_BAT			28
	#define CHK_VBUS	    30

	#define BUTTONS_NUMBER 2

	#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP
	#define BUTTONS_ACTIVE_STATE 0
	
	#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }
	
	#define BSP_BUTTON_0   BUTTON_1
	#define BSP_BUTTON_1   BUTTON_2
#else //ifa
	#define SCL_PIN			16//
	#define PEDO_INT		15//
	#define SDA_PIN			18//
	
	#define HAPTIC_OUT		28//
	#define PMIC_INT		24//
	
	#define PEDO_ASCK_PIN		12
	#define PEDO_ASDA_PIN		14

	#define LED_1		5//
	#define LED_2		0//
	#define LED_3		1//
	#define LED_4		6//
	#define LED_5		8//
	
	#define BUTTONS_NUMBER	1

	#define BUTTON_PULL	NRF_GPIO_PIN_PULLUP
	#define BUTTONS_ACTIVE_STATE 0
	
	#define BUTTON_1	30//no
	
	#define BUTTONS_LIST { BUTTON_1 }
	
	#define BSP_BUTTON_0	BUTTON_1
#endif
	
#else
	
	// LEDs definitions for PCA10040
	#define LEDS_NUMBER    5

	#define LED_START      17
	#define LED_1          17
	#define LED_2          19
	#define LED_3          20
	#define LED_4          21
	#define LED_5          21
	//#define LED_2          18
	//#define LED_3          19
	//#define LED_4          20
	//#define LED_5          20
	//#define LED_STOP       20

	#define LEDS_ACTIVE_STATE 0

	#define LEDS_INV_MASK  LEDS_MASK

	#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5 }

	#define BSP_LED_0      LED_1
	#define BSP_LED_1      LED_2
	#define BSP_LED_2      LED_3
	#define BSP_LED_3      LED_4
	#define BSP_LED_4      LED_5

	#define BUTTONS_NUMBER 3

	#define BUTTON_1       13
	#define BUTTON_2       14
	#define BUTTON_3       15
	
	#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP
	#define BUTTONS_ACTIVE_STATE 0
		
	#define BUTTONS_LIST { BUTTON_1, BUTTON_2, BUTTON_3 }
	
	#define BSP_BUTTON_0   BUTTON_1
	#define BSP_BUTTON_1   BUTTON_2
	#define BSP_BUTTON_2   BUTTON_3
	//#define BSP_BUTTON_3   BUTTON_4
	
	#define SDA_PIN			18//26//5//6
	#define SCL_PIN			16//27//0//8																																				`

	#define HAPTIC_OUT		12
	
	#define CHK_VBUS	    29
	#define PEDO_INT		1//30 //30 : pcb,  1 : mockup

	#define PMIC_INT		24//
#endif

// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_RC,            \
                                 .rc_ctiv       = 10,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_250_PPM}

#ifdef __cplusplus
}
#endif

#endif // PIN_SGNL_H
