#ifndef _PWM_EVENT_H_
#define _PWM_EVENT_H_

#define PWM_LED_INDEX	5

/*!
 * @brief        This is pwm enable function
 *
 * @retval       N/A
 */
extern void pwm_enable(void);

/*!
 * @brief        This is pwm disable function
 *
 * @retval       N/A
 */
extern void pwm_disable(void);

/*!
 * @brief        This is pwm off function
 *
 * @retval       N/A
 */
extern void pwm_off(void);

/*!
 * @brief        This is pwm initialize function
 *
 * @retval       N/A
 */
extern void pwm_init(void);

/*!
 * @brief        This function is  led1-5 controller
 *
 * @retval       N/A
 */
extern void pwm_led_1(uint32_t ui32_duty);
extern void pwm_led_2(uint32_t ui32_duty);
extern void pwm_led_3(uint32_t ui32_duty);
extern void pwm_led_4(uint32_t ui32_duty);
extern void pwm_led_5(uint32_t ui32_duty);

#endif //_PWM_EVENT_H_
