#ifndef _APP_CONFIG_MANAGER_H_
#define _APP_CONFIG_MANAGER_H_

/*!
 * @brief        This function do initailize of default app configuration memory
 *
 * @retval       N/A
 */
void app_config_default( void );

/*!
 * @brief        This function do write flash for app configuration
 *
 * @retval       N/A
 */
void app_config_write( void );

/*!
 * @brief        This function do read flash for current of app configuration
 *
 * @retval       N/A
 */
void app_config_read( void );

#endif //_APP_CONFIG_MANAGER_H_