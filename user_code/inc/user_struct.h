#ifndef _USER_STRUCT_H_
#define _USER_STRUCT_H_

/**@brief Structure for managing a bit flags */
typedef struct bitflag_{
	volatile uint32_t b1_reset_flag:1;

	volatile uint32_t b1_pedo_int:1;
	volatile uint32_t b1_pedo_success:1;
	
	volatile uint32_t b1_incoming_call:1;
	volatile uint32_t b1_haptic_off:1;
	volatile uint32_t b1_ancs_enable:1;
	
	volatile uint32_t b1_send_package:1;

	volatile uint32_t b1_vchg_status:1;
}bitflag_t;

extern bitflag_t g_str_bit;


typedef struct button_timer_{
	
	volatile uint32_t ui32_button_value;
}button_timer_t;
extern button_timer_t g_str_button;

/**@brief Structure for managing a device's time */
typedef struct clock_timer_{
	
	volatile uint32_t ui32_date; //yymmddhh

	volatile uint8_t ui8_minute;
	
	volatile uint8_t ui8_day_of_week;
}clock_timer_t;

extern clock_timer_t g_str_time;

/**@brief enum for managing a activity status */
typedef enum pedo_mode_{
	PEDO_MOVE_STOP = 0,
	PEDO_MOVE_START,
	PEDO_MOVE_WALK, 
	PEDO_MOVE_RUN,
	PEDO_MOVE_BIKE,
	PEDO_MOVE_VEHICLE,
}pedo_mode_e;

/**@brief Structure for managing a pedometer status */
typedef struct pedo_struct_{
	uint32_t ui32_pedo_step;
	uint32_t ui32_cur_step;
	uint32_t ui32_last_step;
	
	uint32_t ui32_walk_step;
	uint32_t ui32_run_step;
	uint32_t ui32_bike_step;
	uint32_t ui32_vehicle_step;
	
	uint32_t ui32_last_end_step;
	
	uint16_t ui16_walk_time;
	uint16_t ui16_run_time;
	uint16_t ui16_bike_time;
	uint16_t ui16_vehicle_time;
	uint16_t ui16_pre_move_time;
	
	uint16_t ui16_start_walk_time;
	uint16_t ui16_start_run_time;
	uint16_t ui16_start_bike_time;
	uint16_t ui16_start_vehicle_time;
	uint16_t ui16_start_move_time;
	
	uint16_t ui16_pedo_err_cnt;
	
	pedo_mode_e pedo_mode;
	
}pedo_struct_t;

extern pedo_struct_t g_str_pedo;

/**@brief Structure for managing activity */
typedef struct health_struct_{
	uint16_t ui16_longsit_start_time;
	uint16_t ui16_longsit_end_time;
	
	uint16_t ui16_act_time;
	uint16_t ui16_act_step;
	
	uint8_t ui8_longsit_count;
	
}health_struct_t;

/**@brief Structure for managing a notification status */
typedef struct status_config_{
	uint8_t b1_app_noti:1;
	
	uint8_t b1_dnd_status:1;
	
	uint8_t b1_longsit_status:1;
	uint8_t b1_act_step_status:1;
	uint8_t b1_act_time_status:1;
	
	uint8_t b1_act_step_noti:1;
	uint8_t b1_act_time_noti:1;

}status_config_t;

typedef struct app_config_{
	status_config_t flag;
	health_struct_t health;
}app_config_t;

extern app_config_t g_str_app;

/**@brief Structure for managing a battery status */
typedef struct batt_struct_{
	uint16_t ui16_voltage;
	
	uint8_t ui8_sample_percent;
	uint8_t ui8_percentage;
	uint8_t ui8_batt_level;
	
	uint8_t ui8_batt_filter[8];
}batt_struct_t;

extern batt_struct_t g_str_batt;

/**@brief enum for managing a pattern of LED */
typedef enum {
	PATTERN_NONE = 0,
	PATTERN_POWER_ON,
	PATTERN_POWER_OFF,
	
	PATTERN_CHARGE_START,
	PATTERN_CHARGING,
	PATTERN_CHARGE_COMPLETE,
	
	PATTERN_BATTERY_LOW,
	PATTERN_BATTERY_CHECK,
	
	PATTERN_HEALTH_NOTI,
		
	PATTERN_INCOMING_CALL,
	
	PATTERN_BLE_ADVERTISING,
	PATTERN_BLE_CONNECTED,
	PATTERN_BLE_DISCONNECTED,
	
	PATTERN_NOTI_APP,

	PATTERN_RESET,
}pattern_type_e;

/**@brief Structure for managing a LED pattern status */
typedef struct pattern_struct_{
	
	pattern_type_e ui8_curr_pattern;
	
	uint16_t ui16_pattern_cnt;
	uint16_t ui16_pattern_check;
	
	uint16_t ui16_led_pwm[5];
}pattern_struct_t;

extern pattern_struct_t g_str_pattern;

/**@brief enum for managing a chager's status */
typedef enum {
	CHARGER_UNPLUG				= 0x00,
	CHARGER_CHARGE_PLUGGED		= 0x01,
	CHARGER_CHARG_IN			= 0x02,
	CHARGER_INCHARGING			= 0x03,
	CHARGER_DONE				= 0x04,
}charge_status_e;

/**@brief enum for managing a battery status */
typedef enum {
	BATT_INIT				= 0x00,
	BATT_CHARGING			= 0x01,
	BATT_FULL				= 0x02,
	BATT_MID				= 0x03,
	BATT_LOW				= 0x04,
	BATT_VERY_LOW			= 0x05,
}batt_status_e;

/**@brief enum for managing a advertising status */
typedef enum{
	ADV_NONE				= 0x00,
	ADV_CONNECTED			= 0x01,
	ADV_WITH_WHITELIST		= 0x02,
	ADV_WITHOUT_WHITELIST	= 0x03,
	ADV_IDLE				= 0x04,
}adv_status_e;

/**@brief Structure for managing a device's status */
typedef struct status_struct_{
	
	charge_status_e	ui8_charge_status;
	batt_status_e	ui8_batt_status;
	adv_status_e	ui8_adv_status;
}status_struct_t;

extern status_struct_t g_str_stat;

#endif //_USER_STRUCT_H_