#ifndef _ANCS_SERVICE_H_
#define _ANCS_SERVICE_H_

void ancs_service_init( void );
void ancs_negative_action( void );

extern ble_ancs_c_t m_ancs_c;

#endif //_ANCS_SERVICE_H_