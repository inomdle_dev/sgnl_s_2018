#ifndef _TIMER_EVENT_H_
#define _TIMER_EVENT_H_

void global_timer_init( void );
void time_update_hour( void );
void time_sync_req( uint8_t * p_data );


void button_timer_stop( void );
void button_timer_start( void );
void button_timer_init( void );

#endif //_TIMER_EVENT_H_