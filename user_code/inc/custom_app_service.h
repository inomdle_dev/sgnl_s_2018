#ifndef _CUSTOM_APP_EVENT_H_
#define _CUSTOM_APP_EVENT_H_

extern void check_custom_queue( void );
extern void ble_data_send( uint8_t* ui8_data, uint16_t length );
extern void custom_app_service_init( void );

extern void custom_app_rx_queue( void );
extern void custom_app_tx_queue( void );

extern void custom_app_tx_push( uint8_t* ui8_byte, uint16_t ui16_length );

extern ble_custom_app_t m_custom_app;

#endif//_CUSTOM_APP_EVENT_H_