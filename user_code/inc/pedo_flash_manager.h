#ifndef _PEDO_FLASH_MANAGER_H_
#define _PEDO_FLASH_MANAGER_H_

typedef enum hour_write_type_{
	PEDO_WRITE_HOUR,
	PEDO_WRITE_LIVE,
	PEDO_WRITE_ERASE,
}hour_write_type_t;

/*!
 * @brief        This function do write flash for activity data every hour
 *
 * @param[in]    type			type of data
 *								 PEDO_WRITE_HOUR : new data
 *								 PEDO_WRITE_LIVE : live backup data
 *								 PEDO_WRITE_ERASE : erase hour data(reset or new day)
 *
 * @retval       N/A
 */
void pedo_flash_hour_write( hour_write_type_t type );

/*!
 * @brief        This function do read flash for activity data of request time(hour)
 *
 * @param[in]    ui32_call_date		request date
 * @param[in]    step_count			step count of requested date
 * @param[in]    walk_time			walk time of requested date
 * @param[in]    run_time			run time of requested date
 *
 * @retval       N/A
 */
void pedo_flash_hour_read( uint32_t ui32_call_date, uint32_t *step_count, uint16_t *walk_time, uint16_t *run_time );

/*!
 * @brief        This function do write flash for activity data every day. Renewed every backup period(2 weeks).
 *
 * @retval       N/A
 */
void pedo_flash_day_write( void );

/*!
 * @brief        This function do read flash for activity data of request date
 *
 * @param[in]    ui32_call_date		request date
 * @param[in]    step_count			step count of requested date
 *
 * @retval       N/A
 */
void pedo_flash_day_read( uint32_t ui32_call_date, uint32_t *ui32_step_count );

/*!
 * @brief        This function do read flash for POR syncronization
 *
 * @retval       N/A
 */
void pedo_flash_sync_read( void );

#endif //_PEDO_FLASH_MANAGER_H_