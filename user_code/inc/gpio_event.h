#ifndef _GPIO_EVENT_H_
#define _GPIO_EVENT_H_

extern void get_battery_voltage(void);

/*!
 * @brief        This function is read charger status pin
 *
 * @retval       N/A
 */
extern void read_charger_status( void );

/*!
 * @brief        This function is controller of haptic enable/disable
 *
 * @retval       N/A
 */
extern void haptic_control( bool enable );

/*!
 * @brief        This is gpio initialize function
 *
 * @retval       N/A
 */
extern void gpio_init( void );
extern void gpio_int_init( int flag );
#endif //_GPIO_EVENT_H_