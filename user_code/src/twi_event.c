/**
 * File name : twi_event.c
 *
 * This file contains the source code for managing TWI(I2C)
 */
#include <stdint.h>

#include "boards.h"

#include "app_util_platform.h"
#include "nrf_delay.h"
#include "nrf_drv_twi.h"

#include "twi_event.h"

#define TWI_TIMEOUT 			10000 

static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(0);

union{
	uint32_t value;
	struct {
		uint32_t  m_xfer_done:1;
	};
} twi_done_check;

void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{   
	twi_done_check.value = 0;
}

/**
 * @brief UART initialization.
 */
void twi_init(void)
{
	ret_code_t err_code;

	nrf_gpio_cfg(
	SCL_PIN,
	NRF_GPIO_PIN_DIR_OUTPUT,
	NRF_GPIO_PIN_INPUT_DISCONNECT,
	NRF_GPIO_PIN_PULLUP,
	NRF_GPIO_PIN_H0H1, // NRF_GPIO_PIN_S0S1,
	NRF_GPIO_PIN_NOSENSE);
	
	nrf_gpio_cfg(
	SDA_PIN,
	NRF_GPIO_PIN_DIR_OUTPUT,
	NRF_GPIO_PIN_INPUT_DISCONNECT,
	NRF_GPIO_PIN_PULLUP,
	NRF_GPIO_PIN_H0H1, // NRF_GPIO_PIN_S0S1,
	NRF_GPIO_PIN_NOSENSE);

#ifdef BOARD_SGNL_S
	nrf_gpio_cfg(
	PEDO_ASCK_PIN,//pedo asck pin
	NRF_GPIO_PIN_DIR_INPUT,
	NRF_GPIO_PIN_INPUT_DISCONNECT,
	NRF_GPIO_PIN_PULLUP,
	NRF_GPIO_PIN_H0H1, // NRF_GPIO_PIN_S0S1,
	NRF_GPIO_PIN_NOSENSE);
	
	nrf_gpio_cfg(
	PEDO_ASDA_PIN,//pedo asda pin
	NRF_GPIO_PIN_DIR_INPUT,
	NRF_GPIO_PIN_INPUT_DISCONNECT,
	NRF_GPIO_PIN_PULLUP,
	NRF_GPIO_PIN_H0H1, // NRF_GPIO_PIN_S0S1,
	NRF_GPIO_PIN_NOSENSE);
#endif
	
	for (int8_t i = 0; i < 9; ++i)
	{
		nrf_gpio_pin_clear(SCL_PIN); // vdd
		nrf_delay_us(2);
		
		nrf_gpio_pin_set(SCL_PIN); // vdd
		nrf_delay_us(1);
	}

	const nrf_drv_twi_config_t twi_config = {
		.scl                = SCL_PIN,
		.sda                = SDA_PIN,
		.frequency          = NRF_TWI_FREQ_400K,
		.interrupt_priority = APP_IRQ_PRIORITY_HIGH,
		.clear_bus_init     = false
	};
	
	err_code = nrf_drv_twi_init(&m_twi, &twi_config, twi_handler, NULL);
	APP_ERROR_CHECK(err_code);
	
	nrf_drv_twi_enable(&m_twi);
	
}

void twi_mul_write(uint8_t i2c_addr, const uint8_t *data, uint8_t length )
{
	static uint8_t control = 0x40;
	uint32_t timeout = TWI_TIMEOUT;
	
	uint8_t reg_data[100];	

	reg_data[0] = control;
	uint8_t i = 0;
	for(i=0; i < length; i = i + 1)
	{
		reg_data[i + 1] = data[i];
	}
	
	twi_done_check.m_xfer_done = true;
	nrf_drv_twi_tx(&m_twi, i2c_addr, reg_data, length+1, false);
	
	while(twi_done_check.m_xfer_done && --timeout)
	{
		__WFE();
	}
	
	if( !timeout )
	{
		twi_done_check.m_xfer_done = false;
		return;// NRF_ERROR_TIMEOUT
	}
}

int twi_write(uint8_t i2c_addr, const uint8_t *data, uint32_t length)
{
	ret_code_t err_code;
	uint32_t timeout = TWI_TIMEOUT;

	twi_done_check.m_xfer_done = true;

	err_code = nrf_drv_twi_tx(&m_twi, i2c_addr, data, length , false);
	APP_ERROR_CHECK(err_code);

	while(twi_done_check.m_xfer_done && --timeout)
	{
		__WFE();
	}
	
	if( !timeout )
	{
		twi_done_check.m_xfer_done = false;
		return NRF_ERROR_TIMEOUT;
	}

	return 0;
}

#if 1//def ENABLE_BHI160

int bhi160_twi_write(uint8_t i2c_addr, uint8_t reg_addr, const uint8_t *data, uint32_t length)
{
	ret_code_t err_code = 0;
	uint32_t timeout = TWI_TIMEOUT;

	uint8_t reg_data[100];
		
	reg_data[0] = reg_addr;
	uint8_t i = 0;
	for(i=0; i < length; i = i + 1)
	{
		reg_data[i + 1] = data[i];
	}

	twi_done_check.m_xfer_done = true;
	err_code = nrf_drv_twi_tx(&m_twi, i2c_addr, reg_data, length+1 , false);
	APP_ERROR_CHECK(err_code);

	while(twi_done_check.m_xfer_done && --timeout)
	{
		__WFE();
	}
	
	if( !timeout )
	{
		twi_done_check.m_xfer_done = false;
		return NRF_ERROR_TIMEOUT;
	}

	return 0;
}

int bhi160_twi_read(uint8_t dev_addr, uint8_t* reg, uint8_t *data, uint32_t length)
{
	ret_code_t err_code;
	uint32_t timeout = TWI_TIMEOUT;
		
	twi_done_check.m_xfer_done = true;
	err_code = nrf_drv_twi_tx(&m_twi, dev_addr, reg, 1, true);
	APP_ERROR_CHECK(err_code);

	while(twi_done_check.m_xfer_done && --timeout)
	{
		__WFE();
	}
	
	if( !timeout )
	{
		twi_done_check.m_xfer_done = false;
		return NRF_ERROR_TIMEOUT;
	}
	
	timeout = TWI_TIMEOUT;
	
	twi_done_check.m_xfer_done = true;
	err_code = nrf_drv_twi_rx(&m_twi, dev_addr, data, length);
	APP_ERROR_CHECK(err_code);

	while(twi_done_check.m_xfer_done && --timeout)
	{
		__WFE();
	}
	
	if( !timeout )
	{
		twi_done_check.m_xfer_done = false;
		return NRF_ERROR_TIMEOUT;
	}

	return 0;
}
#endif

int twi_check_connect( uint16_t i2c_addr )
{
	uint8_t sample_data;

	ret_code_t err_code = nrf_drv_twi_rx(&m_twi, i2c_addr, &sample_data, sizeof(sample_data));
	if (err_code == NRF_SUCCESS)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

