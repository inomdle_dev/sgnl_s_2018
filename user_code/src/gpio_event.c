#include <stdint.h>

#include "boards.h"
#include "nrf_drv_gpiote.h"
#include "nrf_delay.h"

#include "user_struct.h"
#include "gpio_event.h"
#include "timer_event.h"
#include "bhi160.h"

#include "battery_calculator.h"
#include "pattern_control.h"

#define VBUS_PIN_CHECK		1
#define CHARGE_PIN_CHECK		1

/*!
 * @brief        This function is read charger status pin
 *
 * @retval       N/A
 */
static uint8_t key_status = 0;

void get_battery_voltage(void)
{
	uint16_t batt_v;
	uint8_t rx_data[2];
	uint8_t tx_data;
	uint8_t tx_reg;

	bhy_i2c_read((0x90 >> 1), 0x28, rx_data, 1);//battery check //0xf0
	tx_data = 0xf3;
	bhy_i2c_write((0x90 >> 1), 0x28, &tx_data, 1);//battery check //0xf0 (vbat/0.272)
	nrf_delay_us(1000);
	
	bhy_i2c_read((0x6C >> 1), 0x61, rx_data, 2);//battery check //0xf0 (vbat/0.272)
	batt_v = (uint16_t)( ( ( rx_data[0] & 0x0f ) << 8 ) | rx_data[1] );
	
	tx_data = 0xf0;
	bhy_i2c_write((0x90 >> 1), 0x28, &tx_data, 1);//battery check //0xf0 (vbat/0.272)
	
	g_str_batt.ui16_voltage = (uint16_t)((float)batt_v * 3.67647);
}


void read_charger_status( void )
{
#if 0
	if(nrf_drv_gpiote_in_is_set(CHK_VBUS))
	{
		g_str_stat.ui8_charge_status |= VBUS_PIN_CHECK;
		
		if(nrf_drv_gpiote_in_is_set(PMIC_INT))
		{
			g_str_stat.ui8_charge_status &= ~(CHARGE_PIN_CHECK);
		}
		else
		{
			g_str_stat.ui8_charge_status |= CHARGE_PIN_CHECK;
		}
	}
	else
	{
		g_str_stat.ui8_charge_status &= ~(VBUS_PIN_CHECK);
		g_str_stat.ui8_charge_status &= ~(CHARGE_PIN_CHECK);
	}
#else
	uint8_t rx_data;
	uint8_t tx_data;
	uint32_t err_code;

	bhy_i2c_read((0x90 >> 1), 0x03, &rx_data, 1);//status charger B
	if(rx_data & 0x02)//charger attach
	{
		g_str_stat.ui8_charge_status |= CHARGE_PIN_CHECK;
		g_str_bit.b1_vchg_status = 1;
	}
	if(!(rx_data & 0x02))//charger detached
	{
		g_str_stat.ui8_charge_status &= ~(CHARGE_PIN_CHECK);
		g_str_bit.b1_vchg_status = 0;
	}
	if(rx_data & 0x80)
	{
		g_str_stat.ui8_charge_status = CHARGER_DONE;
	}
	
	bhy_i2c_read((0x90 >> 1), 0x01, &rx_data, 1);//charger interrupt refresh
	bhy_i2c_read((0x90 >> 1), 0x07, &rx_data, 1);//charger interrupt refresh
#endif
}

/*!
 * @brief        This function is input pin event handler (pedometer int)
 *
 * @retval       N/A
 */

void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	switch(action)
	{
	case NRF_GPIOTE_POLARITY_TOGGLE:
		if(nrf_drv_gpiote_in_is_set(PEDO_INT))
		{
			g_str_bit.b1_pedo_int = 1;
		}
		break;
	default : break;
	}
}

#if 0
/*!
 * @brief        This function is input pin event handler (VBUS pin int)
 *
 * @retval       N/A
 */
void vbus_in_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	switch(action)
	{
	case NRF_GPIOTE_POLARITY_TOGGLE:
		if(nrf_drv_gpiote_in_is_set(CHK_VBUS))//high
		{
			g_str_stat.ui8_charge_status |= VBUS_PIN_CHECK;
			g_str_bit.b1_vchg_status = 1;
		}
		else//low
		{
			g_str_stat.ui8_charge_status &= ~(VBUS_PIN_CHECK);
		}
		
		battery_info_send();
		break;
	default : break;
	}
}
#endif

/*!
 * @brief        This function is input pin event handler (charger pin int)
 *
 * @retval       N/A
 */

void chg_in_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	switch(action)
	{
	case NRF_GPIOTE_POLARITY_TOGGLE:
		//if(!nrf_drv_gpiote_in_is_set(PMIC_INT))//falling edge ��
		{
			uint8_t rx_data;
			uint8_t tx_data;
			uint32_t err_code;
			bhy_i2c_read((0x90 >> 1), 0x00, &rx_data, 1);//global interrupt
			if(rx_data & 0x04)//key falling edge (pushed key)
			{
				key_status = 1;
				button_timer_start();
			}
			else if(rx_data & 0x08)//key rising edge (released key)
			{
				key_status = 0;
				if(g_str_button.ui32_button_value >= 50)//over 5 seconds
				{
					;//power off
				}
				else
				{
#if 01
					battery_sampling();
#else
					get_battery_voltage();
#endif
					func_btn_click();
				}
				button_timer_stop();
			}
			
			bhy_i2c_read((0x90 >> 1), 0x03, &rx_data, 1);//charger pin
			if(rx_data & 0x02)
			{
				g_str_stat.ui8_charge_status |= CHARGE_PIN_CHECK;
				g_str_bit.b1_vchg_status = 1;
			}
			if(!(rx_data & 0x02))//charger detached
			{
				g_str_stat.ui8_charge_status &= ~(CHARGE_PIN_CHECK);
				g_str_bit.b1_vchg_status = 0;
			}

			bhy_i2c_read((0x90 >> 1), 0x00, &rx_data, 1);//global interrupt refresh			
			bhy_i2c_read((0x90 >> 1), 0x01, &rx_data, 1);//charger interrupt refresh
		}
		break;
	default : break;
	}
}

/*!
 * @brief        This function is controller of haptic enable/disable
 *
 * @retval       N/A
 */
void haptic_control( bool enable )
{
	if( enable )
	{
		if( ( !g_str_app.flag.b1_dnd_status ) && ( !g_str_bit.b1_haptic_off ) )
		{
			nrf_drv_gpiote_out_set(HAPTIC_OUT);
		}
	}
	else
	{
		nrf_drv_gpiote_out_clear(HAPTIC_OUT);
	}
}

/*!
 * @brief        This is gpio initialize function
 *
 * @retval       N/A
 */
void gpio_init( void )
{
	ret_code_t err_code;

	if (!nrf_drv_gpiote_is_init())
	{
    	err_code = nrf_drv_gpiote_init();
   		APP_ERROR_CHECK(err_code);
	}
	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    	//in_config.pull = NRF_GPIO_PIN_NOPULL;
    	in_config.pull = NRF_GPIO_PIN_PULLUP;

	err_code = nrf_drv_gpiote_in_init(PEDO_INT, &in_config, in_pin_handler);
	APP_ERROR_CHECK(err_code);

	nrf_drv_gpiote_in_event_enable(PEDO_INT, true);

	err_code = nrf_drv_gpiote_in_init(PMIC_INT, &in_config, chg_in_handler);
	APP_ERROR_CHECK(err_code);

	nrf_drv_gpiote_in_event_enable(PMIC_INT, true);

	nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE(false);

    err_code = nrf_drv_gpiote_out_init(HAPTIC_OUT, &out_config);
    APP_ERROR_CHECK(err_code);
	
	nrf_drv_gpiote_out_clear(HAPTIC_OUT);
}
