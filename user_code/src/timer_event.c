#include <string.h>

#include "app_timer.h"

#include "user_struct.h"

#include "timer_event.h"
#include "saadc_event.h"

#include "battery_calculator.h"

#include "pattern_control.h"

#include "bhi160.h"
#include "pedo_flash_manager.h"

#define GLOBAL_TIMER_TICKS	APP_TIMER_TICKS(60000)
#define BUTTON_TIMER_TICKS	APP_TIMER_TICKS(100)

APP_TIMER_DEF( m_global_timer_id );
APP_TIMER_DEF( m_button_timer_id );

void day_of_week( void )
{
	uint8_t year	= (uint8_t)(g_str_time.ui32_date>>16);
	uint8_t month	= (uint8_t)((g_str_time.ui32_date&0xff00)>>8);
	uint8_t days	= (uint8_t)(g_str_time.ui32_date & 0xff);

	int total = (year - 1) * 365 + (((year-1) / 4) - ((year-1) / 100) + ((year-1) / 400));
	int m[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};		// last day of month
	m[1] =  year % 4 == 0 && year % 100 != 0 || year % 400 == 0 ? 29 : 28;	// check leap year

	for(int i=1; i <month; i++)
	{
		total += m[i-1];

	}
	total += days;
	
	g_str_time.ui8_day_of_week = total % 7;
}

void time_update_hour( void )
{
	uint32_t year = 0;
	uint32_t month = 0;
	uint32_t days = 0;

	if((++g_str_time.ui32_date & 0xFF) >= 24)// When it is 24 o'clock
	{       
		uint32_t tmp_hour = g_str_time.ui32_date & 0xFF;
		
		year	= (g_str_time.ui32_date&0xff000000)>>24;
		month	= (g_str_time.ui32_date&0xff0000)>>16;
		days	= (g_str_time.ui32_date&0xff00)>>8;

		if( ( (month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12) ) && (days == 31) )
		{
			month++;
			days = 1;
			if(month == 13)
			{
				year++;
				month = 1;
			}
		}
		else if( ( (month == 4) || (month == 6) || (month == 9) || (month == 11) ) && (days == 30) )
		{
			month++;
			days = 1;
		}
		else if( (month == 2) && ( ( ( (year % 4) == 0 ) && (days == 29) ) || ( ( (year % 4) != 0 ) && (days == 28) ) ) )//leap year
		{
			month++;
			days = 1;
		}
		else
		{
			days++;
		}
			
		g_str_time.ui32_date = (year<<24) | (month<<16) | (days<<8);
		g_str_time.ui32_date |= (tmp_hour % 24);

		day_of_week();
	}
	
}

static void global_timer_handler(void * p_context)
{
	if( g_str_time.ui8_minute < 59 )
	{
		g_str_time.ui8_minute++;
	}
	else
	{
		g_str_time.ui8_minute = 0;
		
		if( ( g_str_time.ui32_date & 0xFF ) == 23 ) //new day
		{
			g_str_app.flag.b1_act_step_noti = 1;
			g_str_app.flag.b1_act_time_noti = 1;
			
			pedo_flash_day_write();
			
			memset(&g_str_pedo, 0, sizeof(pedo_struct_t));
			
			bhi160_init();
		}
		else
		{
			pedo_flash_hour_write(PEDO_WRITE_HOUR);
		}
		time_update_hour();
	}
	
	if( g_str_app.flag.b1_longsit_status )
	{
		if( ++g_str_app.health.ui8_longsit_count > 59 )
		{
			pattern_start(PATTERN_HEALTH_NOTI);
			g_str_app.health.ui8_longsit_count = 0;
		}
	}

#if 01
	battery_sampling();
#else
	get_battery_voltage();
#endif
}


void global_timer_start( void )
{
	ret_code_t ret = app_timer_start(m_global_timer_id,
                           GLOBAL_TIMER_TICKS,
                           NULL);
    APP_ERROR_CHECK(ret);
}


void global_timer_init( void )
{
	ret_code_t ret = app_timer_create(&m_global_timer_id,
                           APP_TIMER_MODE_REPEATED,
                           global_timer_handler);
    APP_ERROR_CHECK(ret);
	
	global_timer_start();
}


static void button_timer_handler(void * p_context)
{
	g_str_button.ui32_button_value++;
}


void button_timer_stop( void )
{
	ret_code_t ret = app_timer_stop(m_button_timer_id);
	APP_ERROR_CHECK(ret);

	g_str_button.ui32_button_value = 0;
}

void button_timer_start( void )
{
	ret_code_t ret = app_timer_start(m_button_timer_id,
                           BUTTON_TIMER_TICKS,
                           NULL);
	APP_ERROR_CHECK(ret);

	g_str_button.ui32_button_value = 0;
}


void button_timer_init( void )
{
	ret_code_t ret = app_timer_create(&m_button_timer_id,
                           APP_TIMER_MODE_REPEATED,
                           button_timer_handler);
	APP_ERROR_CHECK(ret);
	
}

void time_sync_req( uint8_t * p_data )
{
	uint32_t ui32_sync_date = p_data[0]<<24 | p_data[1]<<16 | p_data[2]<<8 | p_data[3];
	
	if( ui32_sync_date < g_str_time.ui32_date )
	{
		//device time faster than app
		pedo_flash_hour_write(PEDO_WRITE_ERASE);
	}
	else
	{
		//reset time to sync date
		pedo_flash_hour_write(PEDO_WRITE_HOUR);
	}
	
	g_str_time.ui32_date = ui32_sync_date;
	g_str_time.ui8_minute = p_data[4];
}
