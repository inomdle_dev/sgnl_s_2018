#include <string.h>

#include "nrf_ble_ancs_c.h"
#include "nrf_delay.h"

#include "user_struct.h"
#include "ancs_service.h"

#include "user_gatt_protocol.h"
#include "ancs_flash_manager.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "pattern_control.h"

#define ATTR_DATA_SIZE                 BLE_ANCS_ATTR_DATA_MAX                       /**< Allocated size for attribute data. */

#define DISPLAY_MESSAGE_BUTTON_ID      1                                            /**< Button used to request notification attributes. */


#define BLE_ANCS_C_USER_DEF(_name)                                                                  \
ble_ancs_c_t _name;                                                                          		\
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_ANCS_C_BLE_OBSERVER_PRIO,                                                  \
                     ble_ancs_c_on_ble_evt, &_name)

BLE_ANCS_C_USER_DEF(m_ancs_c);                                                           /**< Apple Notification Service Client instance. */

static ble_ancs_c_evt_notif_t m_notification_latest;                                /**< Local copy to keep track of the newest arriving notifications. */
static ble_ancs_c_attr_t      m_notif_attr_latest;                                  /**< Local copy of the newest notification attribute. */
static ble_ancs_c_attr_t      m_notif_attr_app_id_latest;                           /**< Local copy of the newest app attribute. */

static uint8_t m_attr_appid[ATTR_DATA_SIZE];                                        /**< Buffer to store attribute data. */
static uint8_t m_attr_title[ATTR_DATA_SIZE];                                        /**< Buffer to store attribute data. */

#if 0
static uint8_t m_attr_subtitle[ATTR_DATA_SIZE];                                     /**< Buffer to store attribute data. */
static uint8_t m_attr_message[ATTR_DATA_SIZE];                                      /**< Buffer to store attribute data. */
static uint8_t m_attr_message_size[ATTR_DATA_SIZE];                                 /**< Buffer to store attribute data. */
static uint8_t m_attr_date[ATTR_DATA_SIZE];                                         /**< Buffer to store attribute data. */
static uint8_t m_attr_posaction[ATTR_DATA_SIZE];                                    /**< Buffer to store attribute data. */
static uint8_t m_attr_negaction[ATTR_DATA_SIZE];                                    /**< Buffer to store attribute data. */
static uint8_t m_attr_disp_name[ATTR_DATA_SIZE];                                    /**< Buffer to store attribute data. */
#endif

/**@brief String literals for the iOS notification categories. used then printing to UART. */
static char const * lit_catid[BLE_ANCS_NB_OF_CATEGORY_ID] =
{
    "Other",
    "Incoming Call",
    "Missed Call",
    "Voice Mail",
    "Social",
    "Schedule",
    "Email",
    "News",
    "Health And Fitness",
    "Business And Finance",
    "Location",
    "Entertainment"
};

/**@brief String literals for the iOS notification event types. Used then printing to UART. */
static char const * lit_eventid[BLE_ANCS_NB_OF_EVT_ID] =
{
    "Added",
    "Modified",
    "Removed"
};

/**@brief String literals for the iOS notification attribute types. Used when printing to UART. */
static char const * lit_attrid[BLE_ANCS_NB_OF_NOTIF_ATTR] =
{
    "App Identifier",
    "Title",
    "Subtitle",
    "Message",
    "Message Size",
    "Date",
    "Positive Action Label",
    "Negative Action Label"
};

/**@brief String literals for the iOS notification attribute types. Used When printing to UART. */
static char const * lit_appid[BLE_ANCS_NB_OF_APP_ATTR] =
{
    "Display Name"
};


/**@brief Function for setting up GATTC notifications from the Notification Provider.
 *
 * @details This function is called when a successful connection has been established.
 */
static void apple_notification_setup(void)
{
    ret_code_t ret;

    nrf_delay_ms(100); // Delay because we cannot add a CCCD to close to starting encryption. iOS specific.

    ret = ble_ancs_c_notif_source_notif_enable(&m_ancs_c);
    APP_ERROR_CHECK(ret);

    ret = ble_ancs_c_data_source_notif_enable(&m_ancs_c);
    APP_ERROR_CHECK(ret);

    NRF_LOG_DEBUG("Notifications Enabled.");
	
	g_str_bit.b1_ancs_enable = 1;
}


/**@brief Function for printing an iOS notification.
 *
 * @param[in] p_notif  Pointer to the iOS notification.
 */
static void notif_print(ble_ancs_c_evt_notif_t * p_notif)
{
    NRF_LOG_INFO("\r\nNotification");
    NRF_LOG_INFO("Event:       %s", (uint32_t)lit_eventid[p_notif->evt_id]);
    NRF_LOG_INFO("Category ID: %s", (uint32_t)lit_catid[p_notif->category_id]);
    NRF_LOG_INFO("Category Cnt:%u", (unsigned int) p_notif->category_count);
    NRF_LOG_INFO("UID:         %u", (unsigned int) p_notif->notif_uid);

    NRF_LOG_INFO("Flags:");
    if (p_notif->evt_flags.silent == 1)
    {
        NRF_LOG_INFO(" Silent");
    }
    if (p_notif->evt_flags.important == 1)
    {
        NRF_LOG_INFO(" Important");
    }
    if (p_notif->evt_flags.pre_existing == 1)
    {
        NRF_LOG_INFO(" Pre-existing");
    }
    if (p_notif->evt_flags.positive_action == 1)
    {
        NRF_LOG_INFO(" Positive Action");
    }
    if (p_notif->evt_flags.negative_action == 1)
    {
        NRF_LOG_INFO(" Negative Action");
    }
	
	if ( p_notif->evt_id == BLE_ANCS_EVENT_ID_NOTIFICATION_REMOVED )
	{
		if( p_notif->category_id == BLE_ANCS_CATEGORY_ID_INCOMING_CALL )
		{
			// incoming off
			g_str_bit.b1_incoming_call = 0;
			pattern_all_off();
		}
	}
}


void send_package_name( void )
{
	uint8_t data[32];
	uint8_t ui8_index = 0;
	uint8_t tmp_length = 0;
	
	//send id
	data[++ui8_index] = REPORT_ANCS_DATA;
	data[++ui8_index] = REP_ANCS_DATA_SEND;
	data[++ui8_index] = REP_ANCS_SEND_FIRST_ID;
	
	if( m_notif_attr_app_id_latest.attr_len > 16 )
	{
		tmp_length = 16;
	}
	else
	{
		tmp_length = m_notif_attr_app_id_latest.attr_len;
	}
	
	for( int i = 0; i < tmp_length; i++ )
	{
		data[++ui8_index] = m_notif_attr_app_id_latest.p_attr_data[i];
	}
	data[0] = ++ui8_index;				

	user_gatt_send(data,data[0]);
	
	if( m_notif_attr_app_id_latest.attr_len - tmp_length )
	{
		ui8_index = 0;
		data[++ui8_index] = REPORT_ANCS_DATA;
		data[++ui8_index] = REP_ANCS_DATA_SEND;
		data[++ui8_index] = REP_ANCS_SEND_ADD_ID;
		
		for( int i = tmp_length; i < m_notif_attr_app_id_latest.attr_len; i++ )
		{
			data[++ui8_index] = m_notif_attr_app_id_latest.p_attr_data[i];
		}
		
		data[0] = ++ui8_index;				

		user_gatt_send(data,data[0]);
	}
	
	//send title
	ui8_index = 0;
	data[++ui8_index] = REPORT_ANCS_DATA;
	data[++ui8_index] = REP_ANCS_DATA_SEND;
	data[++ui8_index] = REP_ANCS_SEND_FIRST_TITLE;
	
	if( m_notif_attr_latest.attr_len > 16 )
	{
		tmp_length = 16;
	}
	else
	{
		tmp_length = m_notif_attr_latest.attr_len;
	}
	
	for( int i = 0; i < tmp_length; i++ )
	{
		data[++ui8_index] = m_notif_attr_latest.p_attr_data[i];
	}
	data[0] = ++ui8_index;				

	user_gatt_send(data,data[0]);
	
	if( m_notif_attr_latest.attr_len - tmp_length )
	{
		ui8_index = 0;
		data[++ui8_index] = REPORT_ANCS_DATA;
		data[++ui8_index] = REP_ANCS_DATA_SEND;
		data[++ui8_index] = REP_ANCS_SEND_ADD_TITLE;
		
		for( int i = tmp_length; i < m_notif_attr_latest.attr_len; i++ )
		{
			data[++ui8_index] = m_notif_attr_latest.p_attr_data[i];
		}
		
		data[0] = ++ui8_index;				

		user_gatt_send(data,data[0]);
	}
	
	//send complete
	ui8_index = 0;
	data[++ui8_index] = REPORT_ANCS_DATA;
	data[++ui8_index] = REP_ANCS_DATA_SEND;
	data[++ui8_index] = REP_ANCS_SEND_COMPLETE;
	data[0] = ++ui8_index;				

	user_gatt_send(data,data[0]);
}


/**@brief Function for printing iOS notification attribute data.
 *
 * @param[in] p_attr Pointer to an iOS notification attribute.
 */
static void notif_attr_print(ble_ancs_c_attr_t * p_attr)
{
    if (p_attr->attr_len != 0)
    {
        NRF_LOG_INFO("%s: %s", (uint32_t)lit_attrid[p_attr->attr_id], nrf_log_push((char *)p_attr->p_attr_data));
		
		if( p_attr->attr_id == BLE_ANCS_NOTIF_ATTR_ID_APP_IDENTIFIER )
		{
			if( m_notification_latest.evt_id == BLE_ANCS_EVENT_ID_NOTIFICATION_ADDED )
			{
				if( m_notification_latest.category_id == BLE_ANCS_CATEGORY_ID_INCOMING_CALL )
				{
					// incoming on
					g_str_bit.b1_incoming_call = 1;
					pattern_start(PATTERN_INCOMING_CALL);
				}
				else
				{
					if( ancs_flash_find((char *)p_attr->p_attr_data, p_attr->attr_len) )
					{
						//couldn't find 
						//pattern start
						pattern_start(PATTERN_NOTI_APP);

						// send package name
						g_str_bit.b1_send_package = 1;
					}
					else
					{
						//bloked alarm
					}
				}
			}

		}
		else if( p_attr->attr_id == BLE_ANCS_NOTIF_ATTR_ID_TITLE )
		{
			if( g_str_bit.b1_send_package )
			{
				if( !strncmp((char*)m_notif_attr_app_id_latest.p_attr_data,"com.apple.MobileSMS",m_notif_attr_app_id_latest.attr_len) )
				{
					strncpy((char*)m_notif_attr_latest.p_attr_data, (char*)"Message", 7);
					m_notif_attr_latest.attr_len = 7;
				}
				else if( !strncmp((char*)m_notif_attr_app_id_latest.p_attr_data,"com.apple.mobilemail",m_notif_attr_app_id_latest.attr_len) )
				{
					strncpy((char*)m_notif_attr_latest.p_attr_data, (char*)"Mail", 4);
					m_notif_attr_latest.attr_len = 4;
				}
				
				send_package_name();
				g_str_bit.b1_send_package = 0;
			}
		}
		else if (p_attr->attr_len == 0)
		{
			NRF_LOG_INFO("%s: (N/A)", (uint32_t)lit_attrid[p_attr->attr_id]);
		}
	}
}


/**@brief Function for printing iOS notification attribute data.
 *
 * @param[in] p_attr Pointer to an iOS App attribute.
 */
static void app_attr_print(ble_ancs_c_attr_t * p_attr)
{
    if (p_attr->attr_len != 0)
    {
        NRF_LOG_INFO("%s: %s", (uint32_t)lit_appid[p_attr->attr_id], (uint32_t)p_attr->p_attr_data);
    }
    else if (p_attr->attr_len == 0)
    {
        NRF_LOG_INFO("%s: (N/A)", (uint32_t) lit_appid[p_attr->attr_id]);
    }
}


/**@brief Function for printing out errors that originated from the Notification Provider (iOS).
 *
 * @param[in] err_code_np Error code received from NP.
 */
static void err_code_print(uint16_t err_code_np)
{
    switch (err_code_np)
    {
        case BLE_ANCS_NP_UNKNOWN_COMMAND:
            NRF_LOG_INFO("Error: Command ID was not recognized by the Notification Provider. ");
            break;

        case BLE_ANCS_NP_INVALID_COMMAND:
            NRF_LOG_INFO("Error: Command failed to be parsed on the Notification Provider. ");
            break;

        case BLE_ANCS_NP_INVALID_PARAMETER:
            NRF_LOG_INFO("Error: Parameter does not refer to an existing object on the Notification Provider. ");
            break;

        case BLE_ANCS_NP_ACTION_FAILED:
            NRF_LOG_INFO("Error: Perform Notification Action Failed on the Notification Provider. ");
            break;

        default:
            break;
    }
}


/**@brief Function for handling the Apple Notification Service client.
 *
 * @details This function is called for all events in the Apple Notification client that
 *          are passed to the application.
 *
 * @param[in] p_evt  Event received from the Apple Notification Service client.
 */
static void on_ancs_c_evt(ble_ancs_c_evt_t * p_evt)
{
    ret_code_t ret = NRF_SUCCESS;

    switch (p_evt->evt_type)
    {
        case BLE_ANCS_C_EVT_DISCOVERY_COMPLETE:
            NRF_LOG_DEBUG("Apple Notification Center Service discovered on the server.");
            ret = nrf_ble_ancs_c_handles_assign(&m_ancs_c, p_evt->conn_handle, &p_evt->service);
            APP_ERROR_CHECK(ret);
            apple_notification_setup();
            break;

        case BLE_ANCS_C_EVT_NOTIF:
            m_notification_latest = p_evt->notif;
            notif_print(&m_notification_latest);
			
			//req more details
			ret = nrf_ble_ancs_c_request_attrs(&m_ancs_c, &m_notification_latest);
			APP_ERROR_CHECK(ret);
            break;

        case BLE_ANCS_C_EVT_NOTIF_ATTRIBUTE:
            m_notif_attr_latest = p_evt->attr;
            notif_attr_print(&m_notif_attr_latest);

            if (p_evt->attr.attr_id == BLE_ANCS_NOTIF_ATTR_ID_APP_IDENTIFIER)
            {
                m_notif_attr_app_id_latest = p_evt->attr;
				
				//find app_id
#if 0				
				if (m_notif_attr_app_id_latest.attr_len != 0)
				{
					NRF_LOG_INFO("Request for %s: ", (uint32_t)m_notif_attr_app_id_latest.p_attr_data);
					ret = nrf_ble_ancs_c_app_attr_request(&m_ancs_c,
														  m_notif_attr_app_id_latest.p_attr_data,
														  m_notif_attr_app_id_latest.attr_len);
					APP_ERROR_CHECK(ret);
				}
#endif
            }

            break;
        case BLE_ANCS_C_EVT_DISCOVERY_FAILED:
            NRF_LOG_DEBUG("Apple Notification Center Service not discovered on the server.");
            break;

        case BLE_ANCS_C_EVT_APP_ATTRIBUTE:
			{
            	app_attr_print(&p_evt->attr);
			}
            break;
        case BLE_ANCS_C_EVT_NP_ERROR:
            err_code_print(p_evt->err_code_np);
            break;
        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for handling the Apple Notification Service client errors.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void apple_notification_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


void ancs_service_init( void )
{
	ble_ancs_c_init_t ancs_init_obj;
    ret_code_t        ret;

    memset(&ancs_init_obj, 0, sizeof(ancs_init_obj));

    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_APP_IDENTIFIER,
                                  m_attr_appid,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);
	
    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_TITLE,
                                  m_attr_title,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);

#if 0
    ret = nrf_ble_ancs_c_app_attr_add(&m_ancs_c,
                                      BLE_ANCS_APP_ATTR_ID_DISPLAY_NAME,
                                      m_attr_disp_name,
                                      sizeof(m_attr_disp_name));
    APP_ERROR_CHECK(ret);

    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_MESSAGE,
                                  m_attr_message,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);

    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_SUBTITLE,
                                  m_attr_subtitle,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);

    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_MESSAGE_SIZE,
                                  m_attr_message_size,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);

    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_DATE,
                                  m_attr_date,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);

    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_POSITIVE_ACTION_LABEL,
                                  m_attr_posaction,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);

    ret = nrf_ble_ancs_c_attr_add(&m_ancs_c,
                                  BLE_ANCS_NOTIF_ATTR_ID_NEGATIVE_ACTION_LABEL,
                                  m_attr_negaction,
                                  ATTR_DATA_SIZE);
    APP_ERROR_CHECK(ret);
#endif
    ancs_init_obj.evt_handler   = on_ancs_c_evt;
    ancs_init_obj.error_handler = apple_notification_error_handler;

    ret = ble_ancs_c_init(&m_ancs_c, &ancs_init_obj);
    APP_ERROR_CHECK(ret);
	
}

void ancs_negative_action( void )
{
	if (m_notification_latest.evt_flags.negative_action == true)
	{
		NRF_LOG_INFO("Performing Negative Action.");
		ret_code_t ret = nrf_ancs_perform_notif_action(&m_ancs_c,
											m_notification_latest.notif_uid,
											ACTION_ID_NEGATIVE);
		APP_ERROR_CHECK(ret);
	}
}
