#include <stdint.h>

#include "nrf_drv_saadc.h"

#include "user_struct.h"
#include "saadc_event.h"

#include "battery_calculator.h"

#define ADC_REF_VOLTAGE_IN_MILLIVOLTS   600                                     /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION    6                                       /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS  270                                     /**< Typical forward voltage drop of the diode . */
#define ADC_RES_10BIT                   1024                                    /**< Maximum digital value for 10-bit ADC conversion. */

#define EMPTY_SOC	5
#define FULL_SOC	96

#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_PRE_SCALING_COMPENSATION)


//#define EUM_ADC
static nrf_saadc_value_t adc_buf[2];

static __INLINE uint8_t sgnl_batt_level_in_percent(const uint16_t mvolts)
{
    uint8_t battery_level = 0;

#ifdef EUM_ADC
	//if( dis_charging )
	{
		if (mvolts >= 2400)//4200
		{
			battery_level = 100;
		}
		else if (mvolts > 2220)//3860
		{
			battery_level = 100 - ((2400 - mvolts) * 33) / 340;
		}
		else if (mvolts > 2165)//3760
		{
			battery_level = 67 - ((2220 - mvolts) * 17) / 100;
		}
		else if (mvolts > 2115)//3650
		{
			battery_level = 50 - ((2165 - mvolts) * 4) / 9;
		}
		else if (mvolts > 2100)//3600
		{
			battery_level = 10 - (2115 - mvolts) / 5;
		}
		else
		{
			battery_level = 0;
		}
	}
#else
	if (mvolts >= 4200)//4200
	{
		battery_level = 100;
	}
	else if (mvolts > 4050)//4050
	{
		battery_level = 80 - ((4050 - mvolts) * 20) / 150;
	}
	else if (mvolts > 3900)//3900
	{
		battery_level = 60 - ((3900 - mvolts) * 20) / 150;
	}
	else if (mvolts > 3750)//3750
	{
		battery_level = 40 - ((3750 - mvolts) * 20) / 150;
	}
	else if (mvolts > 3600)//3600
	{
		battery_level = 20 - ((3600 - mvolts) * 20) / 150;
	}
	else//under 3600
	{
		battery_level = 0;
	}
#endif

    return battery_level;
}


/**@brief Function for handling the ADC interrupt.
 *
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
void saadc_event_handler(nrf_drv_saadc_evt_t const * p_event)
{
	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
	{
		nrf_saadc_value_t adc_result;
		uint32_t          err_code;
		int8_t tmp_percent = 0;
		uint8_t tmp_data = 0;
		
		adc_result = p_event->data.done.p_buffer[0];
		
		err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, 1);
		APP_ERROR_CHECK(err_code);
		
#ifdef EUM_ADC
		g_str_batt.ui16_voltage = ADC_RESULT_IN_MILLI_VOLTS(adc_result) +
			DIODE_FWD_VOLT_DROP_MILLIVOLTS;
		tmp_data = sgnl_batt_level_in_percent(g_str_batt.ui16_voltage);
		tmp_percent = (uint16_t)( ( tmp_data - EMPTY_SOC ) * 100 / ( FULL_SOC - EMPTY_SOC ) );
#else
		int16_t batt_v = ADC_RESULT_IN_MILLI_VOLTS(adc_result);
		g_str_batt.ui16_voltage = (uint16_t)((float)batt_v * 3.67647);
		tmp_percent = sgnl_batt_level_in_percent(g_str_batt.ui16_voltage);
#endif
				
		if( tmp_percent > 0 )
		{
			if( tmp_percent < 100 )
			{
				g_str_batt.ui8_sample_percent = tmp_percent;
			}
			else
			{
				g_str_batt.ui8_sample_percent = 100;
			}
		}
		else
		{
			g_str_batt.ui8_sample_percent = 0;
		}
		
		battery_filter();
	}
}


/**@brief Function for configuring ADC to do battery level conversion.
 */
void adc_configure(void)
{
	ret_code_t err_code = nrf_drv_saadc_init(NULL, saadc_event_handler);
	APP_ERROR_CHECK(err_code);
	
	nrf_saadc_channel_config_t config =
		NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN5);
	err_code = nrf_drv_saadc_channel_init(0, &config);
	APP_ERROR_CHECK(err_code);
	
	err_code = nrf_drv_saadc_buffer_convert(&adc_buf[0], 1);
	APP_ERROR_CHECK(err_code);
	
	err_code = nrf_drv_saadc_buffer_convert(&adc_buf[1], 1);
	APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *          This function will start the ADC.
 *
 * @param[in] p_context   Pointer used for passing some arbitrary information (context) from the
 *                        app_start_timer() call to the timeout handler.
 */
void battery_sampling(void)
{
    ret_code_t err_code;
    err_code = nrf_drv_saadc_sample();
    APP_ERROR_CHECK(err_code);
}
