#include <stdint.h>

#include "boards.h"

#include "nrf_drv_gpiote.h"
#include "nrf_drv_twi.h"
#include "ble_custom_app.h"

#include "user_struct.h"

#include "pwm_event.h"
#include "flash_event.h"
#include "saadc_event.h"
#include "gpio_event.h"
#include "twi_event.h"
#include "user_gatt_protocol.h"
#include "nrf_ble_ancs_c.h"
#include "ancs_service.h"

#include "bhi160.h"
#include "pattern_control.h"

#include "app_config_manager.h"
#include "pedo_flash_manager.h"
#include "user_task.h"

bitflag_t g_str_bit;
clock_timer_t g_str_time;
app_config_t g_str_app;
batt_struct_t g_str_batt;
status_struct_t g_str_stat;
pattern_struct_t g_str_pattern;
button_timer_t g_str_button;

void struct_initialize( void )
{
	memset(&g_str_bit, 0, sizeof(bitflag_t));
	memset(&g_str_time, 0, sizeof(clock_timer_t));
	memset(&g_str_app, 0, sizeof(app_config_t));
	memset(&g_str_batt, 0, sizeof(batt_struct_t));
	memset(&g_str_stat, 0, sizeof(status_struct_t));
	memset(&g_str_pattern, 0, sizeof(pattern_struct_t));
	memset(&g_str_button, 0, sizeof(button_timer_t));
}

void user_init( void )
{
	struct_initialize();
	gpio_init();
		
	twi_init();
	pwm_init();
	adc_configure();

	flash_init();

	if( bhi160_init() )
	{
		//fail
		g_str_bit.b1_pedo_success = 0;
	}
	else
	{
		g_str_bit.b1_pedo_success = 1;
	}
	
	//read flash app config /timer /pedometer
	pedo_flash_sync_read();
	app_config_read();
}

void user_task( void )
{
	if( g_str_bit.b1_pedo_success )
	{
		if( g_str_bit.b1_pedo_int || nrf_drv_gpiote_in_is_set(PEDO_INT) )
		{
			bhi160_read_data();
			
			if( nrf_drv_gpiote_in_is_set(PEDO_INT) )
			{
				if( ++g_str_pedo.ui16_pedo_err_cnt > 10 )
				{
					g_str_bit.b1_pedo_success = 0;
					g_str_pedo.ui16_pedo_err_cnt = 0;
				}
			}
			else
			{
				g_str_pedo.ui16_pedo_err_cnt = 0;
			}
		}
		g_str_bit.b1_pedo_int = 0;
	}
	else
	{
#ifdef BOARD_SGNL_S
		g_str_pedo.ui32_last_step = g_str_pedo.ui32_pedo_step;
		if( !bhi160_init() )
		{
			g_str_bit.b1_pedo_success = 1;
			g_str_pedo.ui16_pedo_err_cnt = 0;
		}
		else
		{
			g_str_pedo.ui16_pedo_err_cnt++;
		}
#endif
	}
	
	if( g_str_bit.b1_vchg_status )
	{
		if( g_str_stat.ui8_charge_status & CHARGER_CHARGE_PLUGGED )
		{
			pattern_start(PATTERN_CHARGE_START);
		}
		g_str_bit.b1_vchg_status = 0;
	}
	
	check_gatt_msg();
}


void func_btn_click( void )
{
	if( g_str_bit.b1_incoming_call )
	{
		if( g_str_bit.b1_haptic_off )
		{
			uint8_t ui8_send_data[10];
			uint8_t ui8_index = 0;

			g_str_bit.b1_haptic_off = 0;
			g_str_bit.b1_incoming_call = 0;
			
			//ble send
			ui8_send_data[++ui8_index] = REPORT_CALL_STATUS;
			ui8_send_data[++ui8_index] = REP_CALL_END;
			
			ui8_send_data[0] = ++ui8_index;
			user_gatt_send(ui8_send_data,ui8_index);
			//negative action
			ancs_negative_action();
		}
		else
		{
			g_str_bit.b1_haptic_off = 1;
		}
	}
	else
	{
		pattern_start(PATTERN_BATTERY_CHECK);
	}
}

void factory_reset( void )
{
	pattern_start(PATTERN_RESET);
	//delete flash
	for( int i = 0; i < USER_FLASH_LIST; i++ )
	{
		user_flash_erase( (user_flash_list_t)(i) );
	}
}
