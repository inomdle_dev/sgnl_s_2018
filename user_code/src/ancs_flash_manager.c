#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "flash_event.h"
#include "user_gatt_protocol.h"
#include "ancs_flash_manager.h"

#define PACKAGE_NAME_INDEX	100

/*!
 * @brief        This function do make it lowercase
 *
 * @param[in]    ui8_data			target string
 * @param[in]    ui8_len			length of string
 *
 * @retval       N/A
 */
static void user_str_lower( uint8_t *ui8_data, uint8_t ui8_len )
{
    int i = 0;
    for( i = 0; i < ui8_len; i++ )
    {
        if( ( ui8_data[i] >= 'A' ) && ( ui8_data[i] <= 'Z' ) )
        {
            ui8_data[i] += 0x20;
        }
    }
}

/*!
 * @brief        This function do write flash for ancs package name
 *
 * @param[in]    package_name		address of app package name string
 * @param[in]    package_length		length of app package name
 * @param[in]    write_cnt			data index to be written
 *
 * @retval       N/A
 */
void ancs_flash_write( char *package_name, uint8_t package_length, uint8_t write_cnt )
{
	static uint32_t stored_package_name[8] = {0,};
	
	memset(&stored_package_name, 0, sizeof(stored_package_name));
	strncpy((char *)stored_package_name, (char *)package_name, package_length );
	user_flash_write(USER_FLASH_ANCS_BLOCK, stored_package_name, write_cnt, (uint32_t)sizeof(stored_package_name));
}

/*!
 * @brief        This function do erase flash data of blocked app
 *
 * @param[in]    package_name		address of app package name string
 * @param[in]    package_length		length of app package name
 *
 * @retval       N/A
 */
void ancs_flash_erase( char * package_name, uint8_t package_length )
{
	static uint32_t ancs_package_backup[PACKAGE_NAME_INDEX][8];
	static uint32_t stored_package_name[8];
	int cnt = 0;
	int backup_cnt = 0;
	int found = 0;
	
	memset(&ancs_package_backup, 0, sizeof(ancs_package_backup));

	while(cnt < PACKAGE_NAME_INDEX)// flash read
	{
		user_flash_read( USER_FLASH_ANCS_BLOCK, stored_package_name, cnt, (uint32_t)sizeof(stored_package_name));

		if(stored_package_name[0] == 0xffffffff)// no data
		{
			break;
		}
		else
		{
			uint8_t flash_package_name8[20] = "";
			sprintf((char *)flash_package_name8, "%s",(char *)(stored_package_name));
									
			if( strncmp( (char *)package_name, (char *)flash_package_name8, package_length ) )// 0 : equal, 1: different
			{
				strncpy((char *)(ancs_package_backup[backup_cnt++]), (char *)(stored_package_name), 32);
			}
			else //found
			{
				found = 1;
			}
		}
		cnt++;
	}
	
	if( found )
	{
		user_flash_erase( USER_FLASH_ANCS_BLOCK );		

		if( backup_cnt )
		{
			user_flash_write(USER_FLASH_ANCS_BLOCK, (uint32_t *)&ancs_package_backup[0], 0, (uint32_t)sizeof(ancs_package_backup[0]) * backup_cnt);
		}
	}
}

/*!
 * @brief        This function do find flash data of blocked app list
 *
 * @param[in]    package_name		address of app package name string
 * @param[in]    package_length		length of app package name
 *
 * @retval       result of excution - 0 : find, N : couldn't find, 0xEE : error
 */
uint8_t ancs_flash_find( char * package_name, uint8_t package_length )
{
	static uint32_t stored_package_name[8];
	int cnt = 0;
	
	for( cnt = 0; cnt < PACKAGE_NAME_INDEX; cnt++ )// 100
	{
		user_flash_read( USER_FLASH_ANCS_BLOCK, stored_package_name, cnt, (uint32_t)sizeof(stored_package_name));

		if( stored_package_name[0] == 0xffffffff )
		{
			//couldn't find return index
			return ++cnt;
			//break;
		}
		else
		{
			//char stored_package_name_char[32] = "";
			//sprintf(stored_package_name_char, "%s",(char *)(stored_package_name));
			user_str_lower((uint8_t *)package_name, package_length);
			
			if( !strncmp( (char *)package_name, (char *)stored_package_name, package_length ) )// found
			{
				return 0;
				//break;
			}
		}
	}

	return 0xEE;
}

/*!
 * @brief        This function do read number of blocked app list
 *
 * @retval       result of excution - N : number of blocked app lists , 0xEE : error
 */
uint8_t ancs_flash_read_cnt( void )
{
	static uint32_t stored_package_name[8];
	int cnt = 0;
	
	for( cnt = 0; cnt < PACKAGE_NAME_INDEX; cnt++ )// 100
	{
		user_flash_read( USER_FLASH_ANCS_BLOCK, stored_package_name, cnt, (uint32_t)sizeof(stored_package_name));

		if( stored_package_name[0] == 0xffffffff )
		{
			//couldn't find return index
			return cnt;
			//break;
		}
	}

	return 0xEE;
}

/*!
 * @brief        This function do parsing data of blocked app name
 *
 * @param[in]    p_data				target data of parsing
 * @param[in]    ui8_data_len		length of target data
 *
 * @retval       N/A
 */
void ancs_data_parser( uint8_t * p_data, uint8_t ui8_data_len )
{
	static char ui8_package_name[32] = "";
	static uint8_t ui8_length = 0;
	
	uint8_t u8_status = p_data[0];
	
	switch( u8_status )
	{
	case REP_ANCS_SEND_FIRST_ID :
		memset(&ui8_package_name, 0, sizeof(ui8_package_name));
		strncpy(ui8_package_name, (char *)(&p_data[1]), ui8_data_len);
		
		ui8_length = ui8_data_len;
		break;
		
	case REP_ANCS_SEND_ADD_ID :
		if( ui8_length < 32 )
		{
			strncpy(&ui8_package_name[ui8_length], (char *)(&p_data[1]), ui8_data_len);
			
			ui8_length += ui8_data_len;
		}
		break;
		
	case REP_ANCS_SEND_COMPLETE :
		{
			uint8_t ui8_type = p_data[1];
			switch( ui8_type )
			{
			case REP_ANCS_FLAG_WRITE :
				{
					uint8_t write_cnt = ancs_flash_read_cnt();
					
					if( write_cnt != 0xEE )//flash remain
					{
						ancs_flash_write(ui8_package_name, ui8_length, write_cnt);
					}
					else //data full
					{
						//send memory full
					}
				}
				break;
			
			case REP_ANCS_FLAG_ERASE :
				ancs_flash_erase(ui8_package_name, ui8_length);
				break;
			
			default : break;
			}
		}
		break;
		
	default : break;
	}
}
