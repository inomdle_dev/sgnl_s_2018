#include <stdint.h>
#include "app_timer.h"
#include "pwm_event.h"
#include "gpio_event.h"
#include "pattern_control.h"

#include "user_struct.h"

#define PATTERN_CONTROL_TICKS	APP_TIMER_TICKS(20)
#define LED_BRIGHTNESS		20
#define LED_BRIGHT_MAX		100

APP_TIMER_DEF(m_pattern_control_timer_id);

static void pattern_power_on( void );
static void pattern_power_off( void );

static void pattern_charge_start( void );
static void pattern_charging( void );
static void pattern_charge_complete( void );

static void pattern_battery_low( void );
static void pattern_battery_check( void );

static void pattern_health_noti( void );

static void pattern_incoming_call( void );

static void pattern_ble_advertising( void );
static void pattern_ble_connected( void );
static void pattern_ble_disconnected( void );

static void pattern_noti_app( void );

static void pattern_reset( void );

p_pattern_arr p_curr_pattern_arr[PATTERN_INDEX] = 
{
	NULL,
	pattern_power_on,
	pattern_power_off,
	
	pattern_charge_start,
	pattern_charging,
	pattern_charge_complete,
	
	pattern_battery_low,
	pattern_battery_check,
	
	pattern_health_noti,
		
	pattern_incoming_call,
	
	pattern_ble_advertising,
	pattern_ble_connected,
	pattern_ble_disconnected,
	
	pattern_noti_app,

	pattern_reset,
};

p_led_arr p_led_func_arr[PWM_LED_INDEX] = 
{
	pwm_led_1,
	pwm_led_2,
	pwm_led_3,
	pwm_led_4,
	pwm_led_5
};

uint32_t g_ui32_pattern_delay[PATTERN_INDEX] = 
{
	0,
	PATTERN_POWER_ON_CNT,//APP_TIMER_TICKS(3000),//pattern_power_on,
	PATTERN_POWER_OFF_CNT,//pattern_power_off,

	PATTERN_BATT_CHARGING_CNT,//pattern_charge_start,
	PATTERN_BATT_CHARGING_CNT,//pattern_charging,
	PATTERN_BATT_FULL_CNT,//pattern_charge_complete,

	PATTERN_BATT_LOW_CNT,//pattern_battery_low,
	PATTERN_BATT_GAUGE_CNT,//pattern_battery_check,

	PATTERN_APP_NOTI_CNT,//pattern_health_noti,
	PATTERN_APP_NOTI_CNT,//pattern_health_longsit,

	PATTERN_INCOMING_CALL_CNT,//pattern_incoming_call,

	PATTERN_WAIT_PAIRING_CNT,//APP_TIMER_TICKS(3000),//pattern_ble_advertising,
	PATTERN_BLE_CONN_CHK_CNT,//APP_TIMER_TICKS(3000),//pattern_ble_connected,
	PATTERN_DISCONN_TIMEOUT_CNT,//APP_TIMER_TICKS(3000),//pattern_ble_disconnected,

	PATTERN_APP_NOTI_CNT,//pattern_noti_app,

	PATTERN_DEVICE_RESET_CNT,//pattern_reset,
};

static void pattern_control_timer_stop( void );

static void add_pwm( uint16_t index, uint16_t add_val )
{
	if( g_str_pattern.ui16_led_pwm[index] < 100 )
	{
		g_str_pattern.ui16_led_pwm[index] += add_val;
	}
	else
	{
		g_str_pattern.ui16_led_pwm[index] = 100;
	}
	p_led_func_arr[index]( ( g_str_pattern.ui16_led_pwm[index] * LED_BRIGHTNESS ) / LED_BRIGHT_MAX );
}

static void add_pwm_all(uint16_t add_val)
{
	for( int i = 0; i < 5; i++ )
	{
		add_pwm(i, add_val);
	}
}

static void sub_pwm( uint16_t index, uint16_t sub_val )
{
	if( g_str_pattern.ui16_led_pwm[index] > sub_val )
	{
		g_str_pattern.ui16_led_pwm[index] -= sub_val;
	}
	else
	{
		g_str_pattern.ui16_led_pwm[index] = 0;
	}
	
	p_led_func_arr[index]( (g_str_pattern.ui16_led_pwm[index] * LED_BRIGHTNESS ) / LED_BRIGHT_MAX );
}

static void sub_pwm_all(uint16_t sub_val)
{
	for( int i = 0; i < 5; i++ )
	{
		sub_pwm(i, sub_val);
	}
}

static void set_pwm_all( uint16_t val )
{
	for( int i = 0; i < 5; i++ )
	{
		g_str_pattern.ui16_led_pwm[i] = val;
		
		p_led_func_arr[i]( ( g_str_pattern.ui16_led_pwm[i] * LED_BRIGHTNESS ) / LED_BRIGHT_MAX );
	}
}

static void set_pwm( uint16_t index, uint16_t value )
{
	g_str_pattern.ui16_led_pwm[index] = value;
	p_led_func_arr[index]( ( g_str_pattern.ui16_led_pwm[index] * LED_BRIGHTNESS ) / LED_BRIGHT_MAX );
}

static void pattern_control_handler(void * p_context)
{
	if( g_str_pattern.ui8_curr_pattern )
	{
		p_curr_pattern_arr[g_str_pattern.ui8_curr_pattern]();

		if( g_str_pattern.ui16_pattern_cnt == 0 )
		{
			g_str_pattern.ui8_curr_pattern = PATTERN_NONE;
			pattern_control_timer_stop();
		}
		else
		{
			g_str_pattern.ui16_pattern_cnt--;
		}
	}
}

void pattern_timer_init(void)
{
	ret_code_t ret = app_timer_create(&m_pattern_control_timer_id,
									   APP_TIMER_MODE_REPEATED,
									   pattern_control_handler);
    APP_ERROR_CHECK(ret);
}

void pattern_start(uint8_t u8_pattern_index)
{
	if( u8_pattern_index == PATTERN_RESET )
	{
		g_str_pattern.ui8_curr_pattern = PATTERN_NONE;
	}
	else
	{
		if( g_str_pattern.ui8_curr_pattern == u8_pattern_index )
		{
			return;
		}
	}
	
	if( g_str_pattern.ui8_curr_pattern ) //pattern ing
	//if( g_str_pattern.ui16_pattern_cnt ) //pattern ing
	{
		//pattern ready
		g_str_pattern.ui16_pattern_check |= (1 << u8_pattern_index);
	}
	else //pattern none
	{
		g_str_pattern.ui8_curr_pattern = (pattern_type_e)u8_pattern_index;
		g_str_pattern.ui16_pattern_cnt = g_ui32_pattern_delay[u8_pattern_index];
		
		ret_code_t ret	= app_timer_start(m_pattern_control_timer_id,
									  PATTERN_CONTROL_TICKS,
									  NULL);
		APP_ERROR_CHECK(ret);
		
	}
}

void pattern_control_timer_stop( void )
{
	pwm_off();

	g_str_pattern.ui16_pattern_cnt = 0;
		
	ret_code_t ret = app_timer_stop(m_pattern_control_timer_id);
	APP_ERROR_CHECK(ret);
#if 1
	
	if( g_str_pattern.ui16_pattern_check )
	{
		if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_CHARGE_START) )
		{
			pattern_start(PATTERN_CHARGE_START);
			g_str_pattern.ui16_pattern_check = 0;
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_CHARGING) )
		{
			pattern_start(PATTERN_CHARGING);
			g_str_pattern.ui16_pattern_check = 0;
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_CHARGE_COMPLETE) )
		{
			pattern_start(PATTERN_CHARGE_COMPLETE);
			g_str_pattern.ui16_pattern_check = 0;
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_BLE_ADVERTISING) )
		{
			pattern_start(PATTERN_BLE_ADVERTISING);
			g_str_pattern.ui16_pattern_check &= ~(1 << PATTERN_BLE_ADVERTISING);
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_BATTERY_LOW) )
		{
			pattern_start(PATTERN_BATTERY_LOW);
			g_str_pattern.ui16_pattern_check &= ~(1 << PATTERN_BATTERY_LOW);
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_BATTERY_CHECK) )
		{
			pattern_start(PATTERN_BATTERY_CHECK);
			g_str_pattern.ui16_pattern_check &= ~(1 << PATTERN_BATTERY_CHECK);
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_BLE_CONNECTED) )
		{
			pattern_start(PATTERN_BLE_CONNECTED);
			g_str_pattern.ui16_pattern_check &= ~(1 << PATTERN_BLE_CONNECTED);
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_NOTI_APP) )
		{
			pattern_start(PATTERN_NOTI_APP);
			g_str_pattern.ui16_pattern_check &= ~(1 << PATTERN_NOTI_APP);
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_HEALTH_NOTI) )
		{
			pattern_start(PATTERN_HEALTH_NOTI);
			g_str_pattern.ui16_pattern_check &= ~(1 << PATTERN_HEALTH_NOTI);
		}
		else if( g_str_pattern.ui16_pattern_check & (1 << PATTERN_BLE_DISCONNECTED) )
		{
			pattern_start(PATTERN_BLE_DISCONNECTED);
			g_str_pattern.ui16_pattern_check &= ~(1 << PATTERN_BLE_DISCONNECTED);
		}
	}
	else
	{
		if( g_str_stat.ui8_charge_status & CHARGER_CHARGE_PLUGGED )
		{
			pattern_start(PATTERN_CHARGING);
		}
	}
#endif
}

void pattern_all_off( void )
{
	set_pwm_all(0);
	haptic_control(false);
	g_str_pattern.ui16_pattern_cnt = 0;
	pwm_off();
}

void pattern_power_on( void )
{
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_POWER_ON_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 20 )
	{
		add_pwm(0, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 40 )
	{
		set_pwm(0, 100);
		add_pwm(1, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 60 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		add_pwm(2, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 80 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		add_pwm(3, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 100 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		add_pwm(4, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 120 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 140 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 160 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 180 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 200 )
	{
		pattern_all_off();
		return;
	}
	
	// haptic control
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_POWER_ON_CNT )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 25 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

void pattern_power_off( void )
{
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_POWER_OFF_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 40 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 60 )
	{
		sub_pwm(4, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 80 )
	{
		sub_pwm(3, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 100 )
	{
		sub_pwm(2, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 120 )
	{
		sub_pwm(1, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 140 )
	{
		sub_pwm(0, 3);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 160 )
	{
		pattern_all_off();
		return;
	}
	
	// haptic control
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_POWER_OFF_CNT )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 25 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

void pattern_charge_start( void )
{
	// led control
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_BATT_CHARGING_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 20 )
	{
		add_pwm(0, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 40 )
	{
		if( g_str_batt.ui8_batt_level == 0 )
		{
			g_str_pattern.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			g_str_pattern.ui8_curr_pattern = PATTERN_CHARGING;
		}
		add_pwm(1, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 60 )
	{
		if( g_str_batt.ui8_batt_level == 1 )
		{
			g_str_pattern.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			g_str_pattern.ui8_curr_pattern = PATTERN_CHARGING;
		}
		add_pwm(2, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 80 )
	{
		if( g_str_batt.ui8_batt_level == 2 )
		{
			g_str_pattern.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			g_str_pattern.ui8_curr_pattern = PATTERN_CHARGING;
		}
		add_pwm(3, 6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 100 )
	{
		if( g_str_batt.ui8_batt_level == 3 )
		{
			g_str_pattern.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			g_str_pattern.ui8_curr_pattern = PATTERN_CHARGING;
		}
		add_pwm(4, 6);
	}
	else
	{
		if( g_str_stat.ui8_batt_status == BATT_FULL )
		{
			g_str_pattern.ui16_pattern_cnt = 2;
			g_str_pattern.ui8_curr_pattern = PATTERN_CHARGE_COMPLETE;
		}
		else
		{
			g_str_pattern.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			g_str_pattern.ui8_curr_pattern = PATTERN_CHARGING;
		}
	}
	
	// haptic control
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_BATT_CHARGING_CNT )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 25 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

void pattern_charging( void )
{
	if( g_str_stat.ui8_charge_status & CHARGER_INCHARGING )
	{
		if( g_str_stat.ui8_charge_status == CHARGER_CHARGE_PLUGGED )
		{
			g_str_pattern.ui8_curr_pattern = PATTERN_CHARGE_COMPLETE;
			g_str_pattern.ui16_pattern_cnt = 0;
			return;
		}
		else if( g_str_stat.ui8_charge_status == CHARGER_CHARG_IN )
		{
			g_str_pattern.ui8_curr_pattern = PATTERN_NONE;
			g_str_pattern.ui16_pattern_cnt = 0;
			return;
		}
	}
	else
	{
		g_str_pattern.ui8_curr_pattern = PATTERN_NONE;
		g_str_pattern.ui16_pattern_cnt = 0;
		return;
	}
	
	for( int i = 0; i < g_str_batt.ui8_batt_level; i++ )
	{
		set_pwm(i, 100);
	}

	if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 20 )
	{
		sub_pwm(g_str_batt.ui8_batt_level, 6);
		for( int j = g_str_batt.ui8_batt_level+1; j < 5; j++ )
		{
			set_pwm(j, 0);
		}
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 40 )
	{
		add_pwm(g_str_batt.ui8_batt_level, 6);
		for( int j = g_str_batt.ui8_batt_level+1; j < 5; j++ )
		{
			set_pwm(j, 0);
		}
	}
	else
	{
		g_str_pattern.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
	}
}

void pattern_charge_complete( void )
{
	if( g_str_stat.ui8_charge_status & CHARGER_CHARGE_PLUGGED )
	{
		if( g_str_stat.ui8_batt_status == BATT_FULL )
		{
			set_pwm_all(100);
			g_str_pattern.ui16_pattern_cnt = 2;
			return;
		}
	}

	g_str_pattern.ui8_curr_pattern = PATTERN_NONE;
	g_str_pattern.ui16_pattern_cnt = 0;
	set_pwm_all(0);
}

void pattern_battery_low( void )
{
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_BATT_LOW_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 20 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 40 )
	{
		set_pwm(4, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 60 )
	{
		set_pwm(3, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 80 )
	{
		set_pwm(2, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 100 )
	{
		set_pwm(1, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 110 )
	{
		set_pwm(0, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 120 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 130 )
	{
		set_pwm(0, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 140 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 150 )
	{
		set_pwm(0, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 160 )
	{
		set_pwm(0, 100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 170 )
	{
		set_pwm(0, 0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 180 )
	{
		set_pwm(0, 100);
	}
	else
	{
		pattern_all_off();
		return;
	}

	// haptic control
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_BATT_LOW_CNT )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 25 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

void pattern_battery_check( void )
{
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_BATT_GAUGE_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 20 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 40 )
	{
		if( g_str_stat.ui8_charge_status == CHARGER_CHARGE_PLUGGED )
		{
			g_str_pattern.ui16_pattern_cnt = 0;
			set_pwm_all(0);
		}
		else if( g_str_batt.ui8_batt_level < 4 )
		{
			set_pwm(0, 100);
			set_pwm(1, 100);
			set_pwm(2, 100);
			set_pwm(3, 100);
			set_pwm(4, 0);
		}
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 60 )
	{
		if( g_str_batt.ui8_batt_level == 4 )
		{
			g_str_pattern.ui16_pattern_cnt = 100;
			
		}
		else if( g_str_batt.ui8_batt_level < 3 )
		{
			set_pwm(0, 100);
			set_pwm(1, 100);
			set_pwm(2, 100);
			set_pwm(3, 0);
			set_pwm(4, 0);
		}
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 80 )
	{
		if( g_str_batt.ui8_batt_level == 3 )
		{
			g_str_pattern.ui16_pattern_cnt = 100;
		}
		else if( g_str_batt.ui8_batt_level < 2 )
		{
			set_pwm(0, 100);
			set_pwm(1, 100);
			set_pwm(2, 0);
			set_pwm(3, 0);
			set_pwm(4, 0);
		}
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 100 )
	{
		if( g_str_batt.ui8_batt_level == 2 )
		{
			g_str_pattern.ui16_pattern_cnt = 100;

		}
		else if( g_str_batt.ui8_batt_level < 1 )
		{
			set_pwm(0, 100);
			set_pwm(1, 0);
			set_pwm(2, 0);
			set_pwm(3, 0);
			set_pwm(4, 0);
		}
	}
	else if( g_str_pattern.ui16_pattern_cnt < PATTERN_BATT_GAUGE_CNT - 200 )
	{
		pattern_all_off();
		return;
	}
}

	
void pattern_health_noti( void )
{
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_APP_NOTI_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 3 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 10 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 25 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 50 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 75 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 80 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 85 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 100 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 125 )
	{
		set_pwm_all(0);
	}
	else
	{
		pattern_all_off();
		return;
	}
	
	//haptic control
	if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 5 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 20 )
	{
		haptic_control(true);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 30 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 45 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}
	
void pattern_incoming_call( void )
{
	if( !g_str_bit.b1_incoming_call )
	{
		pattern_all_off();
		return;
	}
	
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 20 )
	{
		add_pwm_all(6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 25 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 45 )
	{
		sub_pwm_all(6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 50 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 70 )
	{
		add_pwm_all(6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 75 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 95 )
	{
		sub_pwm_all(6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 100 )
	{
		set_pwm_all(0);
		g_str_pattern.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
	}	
	
	//haptic control
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT - 5 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 35 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

	
void pattern_ble_advertising( void )
{
	if( g_str_stat.ui8_adv_status == ADV_CONNECTED )
	{
		set_pwm_all(0);
		g_str_pattern.ui16_pattern_cnt = 0;
	}
	
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_WAIT_PAIRING_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 20 )
	{
		add_pwm(0, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 40 )
	{
		add_pwm(0, 4);
		add_pwm(1, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 60 )
	{
		sub_pwm(0, 4);
		add_pwm(1, 4);
		add_pwm(2, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 80 )
	{
		sub_pwm(0, 4);
		sub_pwm(1, 4);
		add_pwm(2, 4);
		add_pwm(3, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 100 )
	{
		sub_pwm(1, 4);
		sub_pwm(2, 4);
		add_pwm(3, 4);
		add_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 120 )
	{
		sub_pwm(2, 4);
		sub_pwm(3, 4);
		add_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 140 )
	{
		sub_pwm(3, 4);
		sub_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 160 )
	{
		sub_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 180 )
	{
		add_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 200 )
	{
		add_pwm(3, 4);
		add_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 220 )
	{
		add_pwm(2, 4);
		add_pwm(3, 4);
		sub_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 240 )
	{
		add_pwm(1, 4);
		add_pwm(2, 4);
		sub_pwm(3, 4);
		sub_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 260 )
	{
		add_pwm(0, 4);
		add_pwm(1, 4);
		sub_pwm(2, 4);
		sub_pwm(3, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 280 )
	{
		add_pwm(0, 4);
		sub_pwm(1, 4);
		sub_pwm(2, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 320 )
	{
		sub_pwm(0, 4);
		sub_pwm(1, 4);
	}
	else
	{
		static uint8_t retry = 5;
		if( --retry )
		{
			g_str_pattern.ui16_pattern_cnt = PATTERN_WAIT_PAIRING_CNT;
		}
		else
		{
			pattern_all_off();
			return;
		}
	}
}

void pattern_ble_connected( void )
{
	if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 50 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 75 )
	{
		set_pwm(4,100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 85 )
	{
		set_pwm(0,100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 100 )
	{
		set_pwm(3,100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 110 )
	{
		set_pwm(1,100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 125 )
	{
		set_pwm(2,100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 185 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 150 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 175 )
	{
		pattern_all_off();
		return;
	}
	
	//haptic control
	if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 8 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 15 )
	{
		haptic_control(true);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 25 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 60 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

void pattern_ble_disconnected( void )
{
	//led control
	if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 25 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 75 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 100 )
	{
		set_pwm(2,0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 115 )
	{
		set_pwm(1,0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 125 )
	{
		set_pwm(3,0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 140 )
	{
		set_pwm(0,0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 150 )
	{
		set_pwm(4,0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 175 )
	{
		pattern_all_off();
		return;
	}
	
	//haptic control
	if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 8 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 15 )
	{
		haptic_control(true);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 25 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 30 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

void pattern_noti_app( void )
{
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_APP_NOTI_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 3 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 10 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 25 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 50 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 75 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 80 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 85 )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 100 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 125 )
	{
		set_pwm_all(0);
	}
	else
	{
		pattern_all_off();
		return;
	}
	
	//haptic control
	if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 5 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 20 )
	{
		haptic_control(true);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 30 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 45 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}

void pattern_reset( void )
{
	if( g_str_pattern.ui16_pattern_cnt >= PATTERN_DEVICE_RESET_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 20 )
	{
		add_pwm_all(6);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 30 )
	{
		set_pwm_all(100);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 60 )
	{
		sub_pwm(0, 4);
		sub_pwm(4, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 90 )
	{
		sub_pwm(1, 4);
		sub_pwm(3, 4);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 120 )
	{
		sub_pwm(2, 4);
	}
	else
	{
		g_str_pattern.ui16_pattern_cnt = 0;
		g_str_pattern.ui8_curr_pattern = PATTERN_NONE;
		g_str_pattern.ui16_pattern_check = 0;
		set_pwm_all(0);
		g_str_bit.b1_reset_flag = 1;
	}

	// haptic control
	if( g_str_pattern.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 5 )
	{
		haptic_control(false);
	}
	else if( g_str_pattern.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 25 )
	{
		haptic_control(true);
	}
	else
	{
		haptic_control(false);
	}
}
