#include <stdint.h>

#include "nrf_soc.h"
#include "app_error.h"

#include "user_gatt_protocol.h"
#include "user_struct.h"
#include "saadc_event.h"

#include "battery_calculator.h"
#include "pattern_control.h"

void battery_info_send(void)
{
	uint8_t byte[5] = "";
	uint8_t gatt_index = 0;

	byte[++gatt_index ] = REPORT_APP_CONFIG;
	byte[++gatt_index ] = REP_CONFIG_BATT_STATUS;
	byte[++gatt_index ] = REP_BATT_PERCENTAGE;
	byte[++gatt_index ] = g_str_batt.ui8_percentage;
	byte[0] = ++gatt_index;

	user_gatt_send(byte,byte[0]);
	
	gatt_index= 0;
	byte[++gatt_index] = REPORT_APP_CONFIG;
	byte[++gatt_index] = REP_CONFIG_BATT_STATUS;
	byte[++gatt_index] = REP_BATT_CHARGER;
	byte[++gatt_index] = g_str_stat.ui8_charge_status&0x01;
	byte[0] = ++gatt_index;
	
	user_gatt_send(byte,byte[0]);
	
	if( g_str_stat.ui8_charge_status == CHARGER_UNPLUG )
	{
		if( g_str_batt.ui8_percentage < 1 )
		{
			//power_off
			//power hold pin off
			// Go to system-off mode (this function will not return; wakeup will cause a reset).
			uint32_t ret = sd_power_system_off();
			APP_ERROR_CHECK(ret);
		}
		else
		{
			uint8_t tmp_batt_status = BATT_MID;
			if( g_str_stat.ui8_batt_status != BATT_VERY_LOW )
			{
				if( g_str_batt.ui8_percentage < 5 )
				{
					tmp_batt_status = BATT_VERY_LOW;
					pattern_start(PATTERN_BATTERY_LOW);
				}
			}
			else if( g_str_stat.ui8_batt_status != BATT_LOW )
			{
				if( g_str_batt.ui8_percentage < 10 )
				{
					tmp_batt_status = BATT_LOW;
					pattern_start(PATTERN_BATTERY_LOW);
				}
			}
			g_str_stat.ui8_batt_status = (batt_status_e)tmp_batt_status;
		}
	}
	else
	{
		if( g_str_stat.ui8_charge_status == CHARGER_CHARGE_PLUGGED )
		{
			g_str_batt.ui8_percentage = 100;
			g_str_stat.ui8_batt_status = BATT_FULL;
		}
		else
		{
			g_str_stat.ui8_batt_status = BATT_CHARGING;
		}
	}
}

void battery_filter(void)
{
	uint16_t batt_sum = 0;
	uint8_t tmp_percent = 0;
	
	if( g_str_batt.ui8_batt_filter[7] == 0 )
	{
		for( int i = 0; i < 8; i++ )
		{
			g_str_batt.ui8_batt_filter[i] = g_str_batt.ui8_sample_percent;
		}
		
		g_str_batt.ui8_percentage = g_str_batt.ui8_batt_filter[0];
	}
	else
	{
		g_str_batt.ui8_batt_filter[7] = g_str_batt.ui8_batt_filter[6];
		g_str_batt.ui8_batt_filter[6] = g_str_batt.ui8_batt_filter[5];
		g_str_batt.ui8_batt_filter[5] = g_str_batt.ui8_batt_filter[4];
		g_str_batt.ui8_batt_filter[4] = g_str_batt.ui8_batt_filter[3];
		g_str_batt.ui8_batt_filter[3] = g_str_batt.ui8_batt_filter[2];
		g_str_batt.ui8_batt_filter[2] = g_str_batt.ui8_batt_filter[1];
		g_str_batt.ui8_batt_filter[1] = g_str_batt.ui8_batt_filter[0];
		g_str_batt.ui8_batt_filter[0] = g_str_batt.ui8_sample_percent;;
		
		for( int i = 0; i < 8; i++ )
		{
			batt_sum += g_str_batt.ui8_batt_filter[i];
		}
		
		if( g_str_stat.ui8_charge_status == CHARGER_UNPLUG )
		{
			tmp_percent = ( uint8_t )( batt_sum >> 3 );
			
			if( g_str_batt.ui8_percentage > tmp_percent )
			{
				g_str_batt.ui8_percentage = tmp_percent;
			}
		}
		else
		{
			//charging
			if( g_str_batt.ui8_percentage < 100 )
			{
				g_str_batt.ui8_percentage++;
			}
		}
	}
	
	g_str_batt.ui8_batt_level = g_str_batt.ui8_percentage / 20;
	
	if( g_str_batt.ui8_percentage == 100 )
	{
		if( g_str_stat.ui8_batt_status != BATT_FULL )
		{
			g_str_batt.ui8_batt_level = 4;
		}
	}
	
	battery_info_send();
}