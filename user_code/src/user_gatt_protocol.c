#include <stdint.h>

#include "user_struct.h"

#include "ble_custom_app.h"
#include "custom_app_service.h"

#include "user_gatt_protocol.h"
#include "timer_event.h"
#include "step_counter.h"
#include "pattern_control.h"
#include "battery_calculator.h"
#include "app_config_manager.h"
#include "ancs_flash_manager.h"

void user_gatt_protocol( uint8_t * p_data, uint8_t u8_length )
{
	uint8_t u8_command_id = p_data[1];
	uint8_t u8_data_type = p_data[2];
	
	switch( u8_command_id )
	{
	case REPORT_CALL_STATUS:
		switch( u8_data_type )
		{
		case REP_CALL_INCOMING:
			pattern_start(PATTERN_INCOMING_CALL);
			g_str_bit.b1_incoming_call = 1;
			break;

		case REP_CALL_RECEIVED:
		case REP_CALL_END:
			g_str_bit.b1_incoming_call = 0;
			pattern_all_off();
			break;
		default :
			break;
		}
		break;
		
	case REPORT_NOTIFICATION:
		switch( u8_data_type )
		{
		case REP_NOTI_APP_NOTI:
			pattern_start(PATTERN_NOTI_APP);
			break;
		case REP_NOTI_STEP_NOTI:
		case REP_NOTI_ACT_NOTI:
			pattern_start(PATTERN_HEALTH_NOTI);
			break;
		default :
			break;
		}
		break;
		
	case REPORT_PEDOMETER:
		step_counter_req(&p_data[1]);
		break;
		
	case REPORT_APP_CONFIG:
		{
			uint8_t u8_status = p_data[3];
			switch( u8_data_type )
			{
			case REP_CONFIG_APP_NOTI_STATUS:
				g_str_app.flag.b1_app_noti = u8_status;
				break;
			case REP_CONFIG_DND_STATUS:
				g_str_app.flag.b1_dnd_status = u8_status;
				break;
			case REP_CONFIG_LONGSIT_STATUS:
				{
					//uint8_t u8_param = p_data[4];
					switch(u8_status)
					{
					case REP_LONGSIT_CONFIG:
						g_str_app.flag.b1_longsit_status = p_data[4];
						break;
					case REP_LONGSIT_START:
						g_str_app.health.ui16_longsit_start_time = p_data[4]<<8;
						g_str_app.health.ui16_longsit_start_time |= p_data[5];
						break;
					case REP_LONGSIT_STOP:
						g_str_app.health.ui16_longsit_end_time = p_data[4]<<8;
						g_str_app.health.ui16_longsit_end_time |= p_data[5];
						break;
					}
				}
				break;
			
			case REP_CONFIG_STEP_STATUS:
				g_str_app.flag.b1_act_step_status = u8_status;
				g_str_app.flag.b1_act_step_noti = 1;
				break;
			case REP_CONFIG_STEP_CONFIG:
				g_str_app.health.ui16_act_step = u8_status;
				g_str_app.flag.b1_act_step_noti = 1;
				break;
			case REP_CONFIG_ACT_STATUS:
				g_str_app.flag.b1_act_time_status = u8_status;
				g_str_app.flag.b1_act_time_noti = 1;
				break;
			case REP_CONFIG_ACT_CONFIG:
				if( p_data[0] == 4 )
				{
					g_str_app.health.ui16_act_time = p_data[3];
				}
				else if( p_data[0] == 5 )
				{
					g_str_app.health.ui16_act_time = p_data[3]<<8;
					g_str_app.health.ui16_act_time |= p_data[4];
				}
				g_str_app.flag.b1_act_time_noti  = 1;
				break;
			case REP_CONFIG_BATT_STATUS:
				battery_info_send();
				break;
			case REP_CONFIG_BLE_READY: //time sync
				time_sync_req(&p_data[3]);
				break;
			case REP_CONFIG_RESET_REQ:
				//reset
				break;
			case REP_CONFIG_PEDO_CHK:
				{
					uint8_t ui8_send_data[20];
					uint8_t ui8_length = 0;
					
					ui8_send_data[++ui8_length] = u8_command_id;
					ui8_send_data[++ui8_length] = u8_data_type;
					
					ui8_send_data[++ui8_length] = (g_str_time.ui32_date >> 24) & 0x00FF;
					ui8_send_data[++ui8_length] = (g_str_time.ui32_date >> 16) & 0x00FF;
					ui8_send_data[++ui8_length] = (g_str_time.ui32_date >> 8) & 0x00FF;
					ui8_send_data[++ui8_length] = g_str_time.ui32_date & 0x00FF;
					ui8_send_data[++ui8_length] = g_str_time.ui8_minute;
					
					ui8_send_data[++ui8_length] = (g_str_pedo.ui32_pedo_step >> 24) & 0x00FF;
					ui8_send_data[++ui8_length] = (g_str_pedo.ui32_pedo_step >> 16) & 0x00FF;
					ui8_send_data[++ui8_length] = (g_str_pedo.ui32_pedo_step >> 8) & 0x00FF;
					ui8_send_data[++ui8_length] = g_str_pedo.ui32_pedo_step & 0x00FF;

					
					ui8_send_data[0] = ++ui8_length;
					user_gatt_send(ui8_send_data, ui8_length);
				}
				break;
			default :
				break;
			}
			
			app_config_write();
		}
		break;

	case REPORT_ANCS_DATA:
		ancs_data_parser(&p_data[3], p_data[0] - 4);
		break;
	default :
		break;
	}
}

void user_gatt_send( uint8_t* ui8_data, uint16_t length )
{
	ble_data_send( ui8_data, length );
}

void check_gatt_msg( void )
{
	check_custom_queue();
}
