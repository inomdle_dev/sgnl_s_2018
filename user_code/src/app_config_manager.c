#include <stdint.h>
#include <string.h>

#include "user_struct.h"

#include "flash_event.h"

#include "app_config_manager.h"

/*!
 * @brief        This function do initailize of default app configuration memory
 *
 * @retval       N/A
 */
void app_config_defalut( void )
{
	memset(&g_str_app, 0, sizeof(app_config_t));
	g_str_app.flag.b1_app_noti = 1;
	
	//app_config_write();
}

/*!
 * @brief        This function do write flash for app configuration
 *
 * @retval       N/A
 */
void app_config_write( void )
{
	user_flash_write( USER_FLASH_APP_CONFIG, (uint32_t *)&g_str_app, 0, sizeof(app_config_t));
}

/*!
 * @brief        This function do read flash for current of app configuration
 *
 * @retval       N/A
 */
void app_config_read( void )
{
	user_flash_read( USER_FLASH_APP_CONFIG, (uint32_t *)&g_str_app, 0, sizeof(app_config_t) );
	if( g_str_app.health.ui8_longsit_count == 0xFF )
	{
		app_config_defalut();
	}
	else
	{
		if( g_str_app.flag.b1_act_step_status )
		{
			if( g_str_app.health.ui16_act_step > g_str_pedo.ui32_pedo_step )
			{
				g_str_app.flag.b1_act_step_noti = 1;
			}
			else
			{
				g_str_app.flag.b1_act_step_noti = 0;
			}
		}
		
		if( g_str_app.flag.b1_act_time_status )
		{
			uint16_t tmp_time = g_str_pedo.ui16_walk_time + g_str_pedo.ui16_run_time;
			if( g_str_app.health.ui16_act_time > tmp_time )
			{
				g_str_app.flag.b1_act_time_noti = 1;
			}
			else
			{
				g_str_app.flag.b1_act_time_noti = 0;
			}
		}
		
		g_str_app.health.ui8_longsit_count = 0;
	}
}

