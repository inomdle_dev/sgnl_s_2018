#include <stdint.h>

#include "ble_custom_app.h"
#include "ble_gatts.h"
#include "nrf_queue.h"

#include "pwm_event.h"
#include "custom_app_service.h"
#include "user_gatt_protocol.h"
#include "battery_calculator.h"

#define ENABLE_CUSTOM_APP_QUEUE__

#ifdef ENABLE_CUSTOM_APP_QUEUE
#define CUSTOM_APP_QUEUE_SIZE	30
#endif

#define CUSTOM_APP_DATA_MAX	20

#define BLE_CUSTOM_APP_DEF_USER(_name)													\
ble_custom_app_t _name;																\
NRF_SDH_BLE_OBSERVER(_name ## _obs,												\
					3,															\
					ble_custom_app_on_ble_evt, &_name)

BLE_CUSTOM_APP_DEF_USER(m_custom_app);

uint8_t ui8_ble_send_wait = 0;

#ifdef ENABLE_CUSTOM_APP_QUEUE
typedef struct custom_app_queue_data_{
	uint8_t data[CUSTOM_APP_DATA_MAX];
	uint16_t length;
}custom_app_queue_data_t;

custom_app_queue_data_t custom_app_tx_q_msg;
custom_app_queue_data_t custom_app_tx_pedo;
static int custom_app_tx_q_cnt = 0;

NRF_QUEUE_DEF(custom_app_queue_data_t, m_custom_app_tx_q, CUSTOM_APP_QUEUE_SIZE, NRF_QUEUE_MODE_NO_OVERFLOW);
NRF_QUEUE_INTERFACE_DEC(custom_app_queue_data_t, custom_app_tx_q);
NRF_QUEUE_INTERFACE_DEF(custom_app_queue_data_t, custom_app_tx_q, &m_custom_app_tx_q);

void custom_app_tx_queue( void )
{
	if( custom_app_tx_pedo.length )
	{
		uint32_t err_code = ble_custom_app_string_send(&m_custom_app, custom_app_tx_pedo.data, &custom_app_tx_pedo.length);
		if (err_code != NRF_ERROR_INVALID_STATE)
		{
			APP_ERROR_CHECK(err_code);
		}
		
		//memset(&custom_app_tx_pedo, 0, sizeof(custom_app_queue_data_t) );
	}
	else
	{
		if( custom_app_tx_q_cnt )
		{
			ui8_ble_send_wait = 1;
			memset(&custom_app_tx_q_msg, 0, sizeof(custom_app_queue_data_t) );
			custom_app_tx_q_pop(&custom_app_tx_q_msg);

			uint32_t err_code = ble_custom_app_string_send(&m_custom_app, custom_app_tx_q_msg.data, &custom_app_tx_q_msg.length);
			if (err_code != NRF_ERROR_INVALID_STATE)
			{
				APP_ERROR_CHECK(err_code);
			}

			custom_app_tx_q_cnt--;
		}
	}
}


void custom_app_tx_push( uint8_t* ui8_byte, uint16_t ui16_length )
{
	if( m_custom_app.is_notification_enabled )
	{
		for( int i = 0; i < ui16_length; i++ )
		{
			custom_app_tx_q_msg.data[i] = ui8_byte[i];
		}
		custom_app_tx_q_msg.length = ui16_length;
		custom_app_tx_q_push(&custom_app_tx_q_msg);

		if( custom_app_tx_q_cnt < CUSTOM_APP_QUEUE_SIZE )
		{
			custom_app_tx_q_cnt++;
		}
	}
}

#endif

void ble_data_send( uint8_t* ui8_data, uint16_t length )
{
#ifdef ENABLE_CUSTOM_APP_QUEUE
	if( custom_app_tx_q_cnt || ui8_ble_send_wait || !m_custom_app.is_notification_enabled )
	{
		if( ui8_data[1] == REPORT_PEDOMETER && ui8_data[2] == REP_PEDO_LIVE_DATA )
		{
			for( int i = 0; i < length; i++ )
			{
				custom_app_tx_pedo.data[i] = ui8_data[i];
			}
			custom_app_tx_pedo.length = length;
		}
		else
		{
			custom_app_tx_push(ui8_data, length);
		}
	}
	else
	{
		ui8_ble_send_wait = 1;
		uint32_t err_code = ble_custom_app_string_send(&m_custom_app, ui8_data, &length);
		if (err_code != NRF_ERROR_INVALID_STATE)
		{
			APP_ERROR_CHECK(err_code);
		}
	}
#else
	uint32_t       err_code;

	if( m_custom_app.is_notification_enabled )
	{
		ui8_ble_send_wait = 1;
		err_code = ble_custom_app_string_send(&m_custom_app, ui8_data, &length);
		if (err_code != NRF_ERROR_INVALID_STATE)
		{
			APP_ERROR_CHECK(err_code);
		}
	}
#endif
}

void custom_app_data_handler(ble_custom_app_evt_t * p_evt)
{
	if( p_evt->type == BLE_CUSTOM_APP_EVT_COMM_STARTED )
	{
		pwm_off();
		//nus started
		uint8_t data[3];
		uint8_t ui8_index = 0;
		data[++ui8_index] = REPORT_APP_CONFIG;
		data[++ui8_index] = REP_CONFIG_BLE_READY;
		data[0] = ++ui8_index;				

		user_gatt_send(data,data[0]);
		
		battery_info_send();
		return;
	}
	else if( p_evt->type == BLE_CUSTOM_APP_EVT_COMM_STOPPED )
	{
		//nus stopped
	}

	else if( p_evt->type == BLE_CUSTOM_APP_EVT_TX_RDY )
	{
		//tx complete
//#ifdef ENABLE_CUSTOM_APP_QUEUE
		ui8_ble_send_wait = 0;
//#endif
	}

	else if (p_evt->type == BLE_CUSTOM_APP_EVT_RX_DATA)
    {
		if(p_evt->params.rx_data.length > CUSTOM_APP_DATA_MAX)
		{
			p_evt->params.rx_data.length = CUSTOM_APP_DATA_MAX;
			return;
		}

		user_gatt_protocol((uint8_t *)p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);
	}
}

void check_custom_queue( void )
{
#ifdef ENABLE_CUSTOM_APP_QUEUE
	if( m_custom_app.is_notification_enabled )
	{
		if( !ui8_ble_send_wait )
		{
			custom_app_tx_queue();
		}
	}
#endif
}

void custom_app_service_init(void)
{
    uint32_t       err_code;
    ble_custom_app_init_t custom_app_init;

    custom_app_init.data_handler = custom_app_data_handler;

    err_code = ble_custom_app_init(&m_custom_app, &custom_app_init);
    APP_ERROR_CHECK(err_code);
}
