#include <stdint.h>

#include "user_struct.h"

#include "bhi160.h"

#include "pedo_flash_manager.h"
#include "step_counter.h"
#include "user_gatt_protocol.h"

void step_counter_req( uint8_t *ui8_data )
{
	uint8_t ui8_type = ui8_data[1];
	uint8_t ui8_send_data[20];	
	uint8_t ui8_length = 0;
	
	uint32_t ui32_step_count = 0;

	uint16_t ui16_walk_time = 0;
	uint16_t ui16_run_time = 0;

/*
	uint8_t ui8_year = ui8_data[2];
	uint8_t ui8_month = ui8_data[3];
	uint8_t ui8_day = ui8_data[4];
	uint8_t ui8_hour = ui8_data[5];
	
	uint32_t uint32_t ui32_call_date = ui8_data[2]<<24 + ui8_data[3]<<16 + ui8_data[4]<<8 + ui8_data[5];
*/
	uint32_t ui32_call_date = ui8_data[2]<<24 | ui8_data[3]<<16 | ui8_data[4]<<8 | ui8_data[5];
	
	for( ui8_length = 0; ui8_length < 5; ui8_length++ )
	{
		ui8_send_data[ui8_length+1] = ui8_data[ui8_length];
	}
	
	if( g_str_time.ui32_date < ui32_call_date )
	{
		//write curr_time data
		if( g_str_time.ui32_date )
		{
				
			if( ( g_str_time.ui32_date & 0xFF00 ) != ( ui32_call_date & 0xFF00 ) ) //new day
			{
				pedo_flash_day_write();
			}
			else
			{
				pedo_flash_hour_write(PEDO_WRITE_HOUR);
			}
		}
		else
		{
			pedo_flash_hour_write(PEDO_WRITE_LIVE);
		}
		//pedo_write
		g_str_time.ui32_date = ui32_call_date;
		g_str_time.ui8_minute = 0;
	}
	
	switch( ui8_type )
	{
	case REP_PEDO_LIVE_DATA :
		ui8_send_data[ui8_length+1] = ui8_data[ui8_length];
		ui8_length++;
		
		//step_counter_read( &ui32_walk_cnt, &ui32_run_cnt, &ui16_walk_time, &ui16_run_time );
		//ui32_step_count = ui32_walk_cnt + ui32_run_cnt;
		
		if( g_str_bit.b1_pedo_success )
		{
			step_counter_read( &ui32_step_count, &ui16_walk_time, &ui16_run_time );
		}
#if 1//def DUMMY_TEST
		else
		{
			static uint32_t add_step = 1871;
			static uint16_t add_time = 408;

			static uint32_t cnt = 0;
			
			add_step += (++cnt&0x07);
			add_time += (++cnt&0x03);
			//dummy data
			ui32_step_count = add_step;
			ui16_walk_time = add_time;
			ui16_run_time = add_time>>1;
		}
#endif
		break;

	case REP_PEDO_PAST_TIME :
		pedo_flash_hour_read( ui32_call_date, &ui32_step_count, &ui16_walk_time, &ui16_run_time );
		ui8_send_data[ui8_length+1] = ui8_data[ui8_length];
		ui8_length++;
		break;		

	case REP_PEDO_PAST_DATE :
		pedo_flash_day_read( ui32_call_date, &ui32_step_count );
		break;

	case REP_PEDO_SYNC_DATA :
		{
			uint32_t ui32_cur_steps = ui8_data[7]<<24 | ui8_data[8]<<16 | ui8_data[9]<<8 | ui8_data[10];
			uint16_t ui16_cur_walk_times = ui8_data[11]<<8 | ui8_data[12];
			uint16_t ui16_cur_run_times = ui8_data[13]<<8 | ui8_data[14];
			
			g_str_time.ui8_minute = ui8_data[6];
			
			if( ui32_cur_steps > g_str_pedo.ui32_pedo_step )
			{
				g_str_pedo.ui32_last_step = ui32_cur_steps;
				g_str_pedo.ui32_pedo_step = g_str_pedo.ui32_last_step + g_str_pedo.ui32_cur_step;
			}
			
			if( ui16_cur_walk_times > g_str_pedo.ui16_walk_time )
			{
				g_str_pedo.ui16_walk_time = ui16_cur_walk_times;
			}
			
			if( ui16_cur_run_times > g_str_pedo.ui16_run_time )
			{
				g_str_pedo.ui16_run_time = ui16_cur_run_times;
			}

			return;
		}
		break;

	default :
		break;
	}
	
	ui8_send_data[++ui8_length] = (ui32_step_count >> 24) & 0x00FF;
	ui8_send_data[++ui8_length] = (ui32_step_count >> 16) & 0x00FF;
	ui8_send_data[++ui8_length] = (ui32_step_count >> 8) & 0x00FF;
	ui8_send_data[++ui8_length] = ui32_step_count & 0x00FF;
	
	if( ui8_type != REP_PEDO_PAST_DATE )
	{
		ui8_send_data[++ui8_length] = (ui16_walk_time >> 8) & 0x00FF;
		ui8_send_data[++ui8_length] = ui16_walk_time & 0x00FF;
		
		ui8_send_data[++ui8_length] = 0;
		ui8_send_data[++ui8_length] = 0;
		ui8_send_data[++ui8_length] = 0;
		ui8_send_data[++ui8_length] = 0;

		ui8_send_data[++ui8_length] = (ui16_run_time >> 8) & 0x00FF;
		ui8_send_data[++ui8_length] = ui16_run_time & 0x00FF;
	}

	ui8_send_data[0] = ++ui8_length;
	user_gatt_send(ui8_send_data, ui8_length);
	
}
