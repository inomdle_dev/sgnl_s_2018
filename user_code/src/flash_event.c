#include <string.h>

#include "nrf_soc.h"

#include "app_error.h"
#include "nrf_fstorage_sd.h"
#include "flash_event.h"

#define PAGE_SIZE_WORDS	4096

/*!
 * @brief        This function is flash stroage event handler
 *
 * @param[in]    p_evt		p_evt->result : NRF_SUCCESS - flash write/erase success
 *
 * @retval       N/A
 */
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);

NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = 0x70000,
    .end_addr   = 0x79fff,
};

/**@brief		Helper function to obtain the last address on the last page of the on-chip flash that
 *				can be used to write user data.
 *
 * @retval		end of flash address
 */
static uint32_t nrf5_flash_end_addr_get()
{
    uint32_t const bootloader_addr = NRF_UICR->NRFFW[0];
    uint32_t const page_sz         = NRF_FICR->CODEPAGESIZE;
    uint32_t const code_sz         = NRF_FICR->CODESIZE;

    return (bootloader_addr != 0xFFFFFFFF ?
            bootloader_addr : (code_sz * page_sz));
}

/**@brief		This function to obtain a page address 
 *
 * @retval		the address of a page.
 */
static uint32_t address_of_page(uint16_t page_num)
{
    return fstorage.start_addr + (page_num * PAGE_SIZE_WORDS);
}

/**@brief		This function do initialize flash
 *
 * @retval		N/A
 */
void flash_init(void)
{
	nrf_fstorage_api_t *p_fs_api;
	p_fs_api = &nrf_fstorage_sd;
	
	fstorage.end_addr = nrf5_flash_end_addr_get();
	fstorage.start_addr = fstorage.end_addr - NRF_FICR->CODEPAGESIZE * USER_FLASH_LIST;
	
	ret_code_t rc = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
    APP_ERROR_CHECK(rc);
}

/*!
 * @brief        This function is flash stroage event handler
 *
 * @param[in]    p_evt		p_evt->result : NRF_SUCCESS - flash write/erase success
 *
 * @retval       N/A
 */
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
        } break;

        default:
            break;
    }
}

/*!
 * @brief        This function do wait flash event
 *
 * @param[in]    p_evt		p_evt->result : NRF_SUCCESS - flash write/erase success
 *
 * @retval       N/A
 */
void wait_for_flash_ready(nrf_fstorage_t const * p_fstorage)
{
    /* While fstorage is busy, sleep and wait for an event. */
    while (nrf_fstorage_is_busy(p_fstorage))
    {
        ret_code_t ret = sd_app_evt_wait();
  		APP_ERROR_CHECK(ret);
    }
}

/*!
 * @brief        This function do erase flash
 *
 * @param[in]    list				page index of flash
 *
 * @retval       N/A
 */
void user_flash_erase(user_flash_list_t list)
{
    ret_code_t rc = nrf_fstorage_erase(&fstorage, address_of_page(list), 1, NULL);
    if (rc != NRF_SUCCESS)
    {
        //error
    }
}

/*!
 * @brief        This function do write flash
 *
 * @param[in]    list				page index of flash
 * @param[in]    ui32_data			data to be written
 * @param[in]    ui32_cnt			data index to be written
 * @param[in]    ui32_len			length of data
 *
 * @retval       N/A
 */
void user_flash_write( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_len )
{
    /* The following code snippet make sure that the length of the data we are writing to flash
     * is a multiple of the program unit of the flash peripheral (4 bytes).
     *
     * In case of non-string piece of data, use the sizeof operator instead of strlen.
     */
	
	uint32_t start_addr = (uint32_t )( address_of_page(list) + (ui32_len * ui32_cnt) );
	
	if( !ui32_cnt )
	{
		user_flash_erase(list);
		
		//wait_for_flash_ready(&fstorage);
	}

    ret_code_t rc = nrf_fstorage_write(&fstorage, start_addr, ui32_data, ui32_len, NULL);
    if (rc != NRF_SUCCESS)
    {
        //error
    }
}

/*!
 * @brief        This function do read flash
 *
 * @param[in]    list				page index of flash
 * @param[in]    ui32_data			data to be read
 * @param[in]    ui32_cnt			data index to be read
 * @param[in]    ui32_len			length of data
 *
 * @retval       N/A
 */
void user_flash_read( user_flash_list_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_len )
{
    ret_code_t rc;
    
	uint32_t start_address = (uint32_t )( address_of_page(list) + (ui32_len * ui32_cnt) );

    /* Read data. */
    rc = nrf_fstorage_read(&fstorage, start_address, ui32_data, ui32_len);
	
	if (rc != NRF_SUCCESS)
    {
        //error
    }
}
