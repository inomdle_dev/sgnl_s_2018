#include <stdint.h>
#include <string.h>

#include "user_struct.h"
#include "timer_event.h"
#include "flash_event.h"
#include "pedo_flash_manager.h"

#define PEDOMETER_DAILY_BACKUP_PERIOD	14 //2 weeks
#define PEDOMETER_DAILY_BACKUP_BUFFER	7 //2 weeks
#define PEDOMETER_HOUR_BACKUP_MAX		30//24 // 24 hours + padding 6

uint16_t g_ui16_pedo_day_cnt = 0;
uint16_t g_ui16_pedo_hour_cnt = 0;

/*!
 * @brief        This function do write flash for activity data every hour
 *
 * @param[in]    type			type of data
 *								 PEDO_WRITE_HOUR : new data
 *								 PEDO_WRITE_LIVE : live backup data
 *								 PEDO_WRITE_ERASE : erase hour data(reset or new day)
 *
 * @retval       N/A
 */
void pedo_flash_hour_write( hour_write_type_t type )
{
	static uint32_t pedometer_data[4] = {0,};
	
	if( type == PEDO_WRITE_ERASE )
	{
		user_flash_erase( USER_FLASH_PEDOMETER_HOUR );
		g_ui16_pedo_hour_cnt = 0;
	}
	else
	{
		//pedometer_data_update();
		if( type == PEDO_WRITE_HOUR )
		{
			pedometer_data[0]  = g_str_time.ui32_date;
		}
		else if( type == PEDO_WRITE_LIVE )
		{
			pedometer_data[0]  = ( uint32_t )( (g_str_time.ui32_date<<8) & 0xFFFFFF00 );
			pedometer_data[0] |= 'L';
		}
		pedometer_data[1] = g_str_pedo.ui32_walk_step + g_str_pedo.ui32_vehicle_step;
		pedometer_data[2] = g_str_pedo.ui32_run_step + g_str_pedo.ui32_bike_step;
		pedometer_data[3] = ( uint32_t )( ( ( ( g_str_pedo.ui16_run_time + g_str_pedo.ui16_bike_time ) << 16 ) | ( g_str_pedo.ui16_walk_time + g_str_pedo.ui16_vehicle_time ) ) );

		user_flash_write( USER_FLASH_PEDOMETER_HOUR, pedometer_data, g_ui16_pedo_hour_cnt, sizeof(pedometer_data));

		g_ui16_pedo_hour_cnt++;
	}
}

/*!
 * @brief        This function do read flash for activity data of request time(hour)
 *
 * @param[in]    ui32_call_date		request date
 * @param[in]    step_count			step count of requested date
 * @param[in]    walk_time			walk time of requested date
 * @param[in]    run_time			run time of requested date
 *
 * @retval       N/A
 */
void pedo_flash_hour_read( uint32_t ui32_call_date, uint32_t *step_count, uint16_t *walk_time, uint16_t *run_time )
{
	static uint32_t pedometer_data[4];

	for( int cnt = 0; cnt < PEDOMETER_HOUR_BACKUP_MAX; cnt++ )//24 - 24 hours 
	{
		user_flash_read( USER_FLASH_PEDOMETER_HOUR, pedometer_data, cnt, (uint32_t)sizeof(pedometer_data));

		if( pedometer_data[0] == 0xffffffff )
		{
			//couldn't find
			break;
		}
		else if( ( pedometer_data[0] & 0x00ff ) == (uint8_t)('L') )
		{
			//live_skip
		}
		else if( ui32_call_date == pedometer_data[0] )
		{
			*step_count = pedometer_data[1] + pedometer_data[2];
			
			*run_time = pedometer_data[3] >> 16;
			*walk_time = (pedometer_data[3] & 0xffff);
			break;
		}
	}
}

/*!
 * @brief        This function do write flash for activity data every day. Renewed every backup period(2 weeks).
 *
 * @retval       N/A
 */
void pedo_flash_day_write( void )
{
	static uint32_t pedo_daily_data[2] = {0,};
	uint16_t ui16_pedo_backup_cnt = 0;
	//pedometer_data_update();
	user_flash_erase( USER_FLASH_PEDOMETER_HOUR );
	
	pedo_daily_data[0]  = ( uint32_t )( g_str_time.ui32_date>>8 );
	pedo_daily_data[1] = g_str_pedo.ui32_pedo_step;

	if( g_ui16_pedo_day_cnt >= PEDOMETER_DAILY_BACKUP_PERIOD )
	{
		static uint32_t ui32_pedo_backup_data[PEDOMETER_DAILY_BACKUP_BUFFER+1][2] = {0,};

		ui32_pedo_backup_data[PEDOMETER_DAILY_BACKUP_BUFFER][0] = pedo_daily_data[0];
		ui32_pedo_backup_data[PEDOMETER_DAILY_BACKUP_BUFFER][1] = pedo_daily_data[1];
		
		ui16_pedo_backup_cnt = g_ui16_pedo_day_cnt - PEDOMETER_DAILY_BACKUP_BUFFER;

		for(int i = 0; i < PEDOMETER_DAILY_BACKUP_BUFFER; i++)
		{
			user_flash_read( USER_FLASH_PEDOMETER_DAILY, ui32_pedo_backup_data[i], ui16_pedo_backup_cnt + i, (uint32_t)sizeof(ui32_pedo_backup_data[0]));
		}

		user_flash_write( USER_FLASH_PEDOMETER_DAILY, (uint32_t*)ui32_pedo_backup_data, 0, (uint32_t)sizeof(ui32_pedo_backup_data));
		
		g_ui16_pedo_day_cnt = PEDOMETER_DAILY_BACKUP_BUFFER;
	}
	else
	{
		user_flash_write( USER_FLASH_PEDOMETER_DAILY, pedo_daily_data, g_ui16_pedo_day_cnt, sizeof(pedo_daily_data));
		g_ui16_pedo_day_cnt++;
	}
	
	g_ui16_pedo_hour_cnt = 0;
			
}

/*!
 * @brief        This function do read flash for activity data of request date
 *
 * @param[in]    ui32_call_date		request date
 * @param[in]    step_count			step count of requested date
 *
 * @retval       N/A
 */
void pedo_flash_day_read( uint32_t ui32_call_date, uint32_t *step_count )
{
	static uint32_t pedometer_data[2];
	uint32_t tmp_date = ui32_call_date >> 8 ;
	
	for( int cnt = 0; cnt < PEDOMETER_DAILY_BACKUP_PERIOD; cnt++ )//14 days
	{
		user_flash_read( USER_FLASH_PEDOMETER_DAILY, pedometer_data, cnt, (uint32_t)sizeof(pedometer_data));

		if( pedometer_data[0] == 0xffffffff )
		{
			//couldn't find
			break;
		}
		else if( tmp_date == pedometer_data[0] )
		{
			*step_count = pedometer_data[1];
			break;
		}
	}
}

/*!
 * @brief        This function do read flash for POR syncronization
 *
 * @retval       N/A
 */
void pedo_flash_sync_read( void )
{
	static uint32_t pedometer_data[4];
	static uint32_t pedometer_day_data[2];
	uint32_t tmp_date;
	
	for( int cnt = 0; cnt < PEDOMETER_HOUR_BACKUP_MAX; cnt++ )//24 - 24 hours 
	{
		user_flash_read( USER_FLASH_PEDOMETER_HOUR, pedometer_data, cnt, (uint32_t)sizeof(pedometer_data));

		if( pedometer_data[0] == 0xffffffff )
		{
			break;
		}
		else
		{
			g_ui16_pedo_hour_cnt++;
			
			if( ( pedometer_data[0] & 0x00ff ) == (uint8_t)('L') )
			{
				g_str_time.ui32_date = ( ( pedometer_data[0]>>8 ) & 0x00FFFFFF );
			}
			else
			{
				g_str_time.ui32_date = pedometer_data[0];
			}
			
			g_str_pedo.ui32_walk_step = pedometer_data[1];
			g_str_pedo.ui32_run_step = pedometer_data[2];
			
			g_str_pedo.ui16_walk_time = ( pedometer_data[3] & 0x00FFFF );
			g_str_pedo.ui16_run_time = ( ( pedometer_data[3] >> 16 ) & 0x00FFFF );
		}
	}
	
	for( int cnt = 0; cnt < PEDOMETER_DAILY_BACKUP_PERIOD; cnt++ )//14 days
	{
		user_flash_read( USER_FLASH_PEDOMETER_DAILY, pedometer_day_data, cnt, (uint32_t)sizeof(pedometer_day_data));

		if( pedometer_day_data[0] == 0xffffffff )
		{
			break;
		}
		else
		{
			g_ui16_pedo_day_cnt++;

			tmp_date = pedometer_day_data[0];
		}
	}
	
	if( !g_ui16_pedo_hour_cnt )
	{
		if( g_ui16_pedo_day_cnt )
		{
			g_str_time.ui32_date = tmp_date<<8;
			g_str_time.ui32_date |= 23;
		}
	}

	time_update_hour();
	
	g_str_pedo.ui32_last_step = g_str_pedo.ui32_walk_step + g_str_pedo.ui32_run_step;
}
